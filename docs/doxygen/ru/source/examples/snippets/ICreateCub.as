﻿void main() { 

	Coat coat;
	// перейти в скульпт-комнату
	coat.room("Sculpt");
	
	// воксель
	Vox vox1;
	// очищаем сцену
	vox1.clearScene();
	vox1.rename("Volume3");

	// перейти к примитивам
	UI ui;
	ui("$SCULP_PRIM");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// получить куб
	CubPrim cube2;
	cube2 = tool.cube();
	
	// инициализация параметров
	cube2.AxisX(Vec3(1.00,0.00,0.00));
	cube2.AxisY(Vec3(0.00,1.00,0.00));
	cube2.AxisZ(Vec3(0.00,0.00,1.00));
	
	// отключить фаску
	cube2.useFillet(false);
	
	cube2.Position(Vec3(0.00,0.00,0.00));
	cube2.SideA(30.00);
	cube2.SideB(30.00);
	cube2.SideC(30.00);
	
	// перейти в сурфексный режим
	vox1.toSurface();
	
	// включить фаску
	cube2.useFillet(true);
	
	// установить радиус
	cube2.filletRadiusRelation(0.20);
	cube2.filletRadius(14.41);
	
	// установить позицию
	cube2.Position(Vec3(-223.11,44.94,336.39));
	
	// стороны
	cube2.SideA(144.11);
	cube2.SideB(144.11);
	cube2.SideC(144.11);
	cube2.details(0.38);
	
	// добавить куб в сцену
	tool.Apply(0);
}
