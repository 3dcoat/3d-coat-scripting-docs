﻿void main() {

   	Coat coat;
	// перейти в скульпт комнату 
	coat.room("Sculpt");
	
	// воксельный объект
	Vox vox1;
	// очищаем сцену
	vox1.clearScene();
	vox1.rename("Volume2");

	// перейти к примитивам
	UI ui;
	ui("$SCULP_PRIM");

	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// создать объект токарный станок
	LathePrim lathe10;
	lathe10 = tool.lathe();
	
	// установить оси
	lathe10.AxisX(Vec3(0.22,0.08,0.97));
	lathe10.AxisY(Vec3(-0.09,0.99,-0.06));
	lathe10.AxisZ(Vec3(-0.97,-0.07,0.23));
	
	// установить позицию
	lathe10.Position(Vec3(-141.10,35.58,306.22));
	
	// стороны
	lathe10.SideA(160.96);
	lathe10.SideB(160.96);
	lathe10.SideC(160.96);
	
	// профиль цилиндрический
	lathe10.LatheType(0);
	
	// координаты точек
	lathe10.SetPoint(0,0.00,0.50,0);
	lathe10.SetPoint(1,0.49,0.11,0);
	lathe10.SetPoint(2,1.00,0.50,0);

	
	// добавить объект в сцену
	tool.Apply(0);
}
