// счётчик файлов
int n;
// список файлов
string files;


void main() {

    n = 0;
    files = "";

    // спросим у художника, хочет ли он запустить нашу обработку
    bool ok = ModalDialogOkCancel( "Start processing files?", "" );
    if ( !ok ) {
        // прекратим выполнение скрипта, если выбран `Cancel`
        return;
    }

    // вызовем функцию `fileProcessing()` для каждого файла
    // по маске `mask` из выбранной папки
    // фильтр для выборки файлов
    // например, для передачи в функцию только PNG и JPG,
    // ставь маску в `*.png;*.jpg`
    string mask = "*.png;*.jpg";
    ForEachFileInFolder( "", mask, "fileProcessing" );
    // по завершению покажем что обработали
    ModalDialog( "Files processed:\n" + files, "" );
}




void fileProcessing( string &in fileName ) {

    ++n;
    files += n + ". " + fileName + "\n";
}
