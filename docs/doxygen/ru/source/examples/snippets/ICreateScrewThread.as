﻿void main() {
	Coat coat;
	UI ui;
	//перейти в скульпт комнату
	coat.room("Sculpt");
	
	Vox vox1;
	//очистить сцену
	vox1.clearScene();
	vox1.rename("Volume3");

	// перейти в сурфейсный пережим
	vox1.toSurface();
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// примитивы
	ui("$SCULP_PRIM");
	// создать резьбу
 	ThreadPrim thread15;
	thread15 = tool.thread();
	// позиция
	thread15.Position(Vec3(-416.06,-69.18,174.47));
	// диаметр
	thread15.Diameter(135.42);
	thread15.Pitch(22.57);
	thread15.Turns(10);
	// профиль треугольный
	thread15.Profile(ProfileType::TRIANGLE);
	thread15.ClockWise(true);
	thread15.StubHeight(4.51);
	// сурфейсный режим
	vox1.toSurface();
	// уровень детализации
	thread15.details(0.59);
	// добавить в сцену
	tool.Apply(0);
}
