﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//prepare the scene
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	
	// goto surface mode
	vox1.toSurface();
	
	// create the tube obj
	TubePrim tube7;
	tube7 = tool.tube();
	
	// init parameters
	tube7.startPoint(Vec3(-223.68,3.57,222.30));
	tube7.endPoint(Vec3(-230.67,118.01,173.44));
	
	tube7.height(124.63);
	tube7.radius(32.72);
	
	tube7.WallThickness(16.36);
	tube7.details(0.84);
	
	// change parameters
	tube7.useFillet(true);
	tube7.Bevel(1,false);
	tube7.Bevel(2,true);
	tube7.filletRadiusRelation(0.20);
	
	tube7.startPoint(Vec3(-227.87,-73.63,238.01));
	tube7.endPoint(Vec3(-217.73,113.49,199.92));
	
	tube7.height(191.24);
	tube7.radius(50.20);
	
	tube7.WallThickness(25.10);
	tube7.details(0.57);
	
	// add in the scene
	tool.Apply(0);
}
