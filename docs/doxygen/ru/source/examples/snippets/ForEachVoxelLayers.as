// доступ к воксельному дереву
Vox v;


void main() {

    // строим дерево v-слоёв
    // см. `CreateVoxelTree.as`
    // ...

    // проходим все слои и вызываем для каждого функцию invert()
    // см. ниже `void invert()`
    v.forRootEach( "invert" );

    // перемещаемся на слой "Body" и вызываем `invert()` для него и его деток
    v.to( "Body" ).call( "invert" ).forEach( "invert" );
}




// функция для `forEach()`, см. выше
void invert() {

    v.isSurface() ? v.toVoxel() : v.toSurface();

    // пауза, чтобы видеть движение в UI
    Wait( 50 );
}
