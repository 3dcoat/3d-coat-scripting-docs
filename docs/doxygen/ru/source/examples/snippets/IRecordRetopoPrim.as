﻿// This file generated after the script recording in the retopo room.
void main() {
	
	Coat coat;
	// goto retopo room
	coat.room("Retopo");
	
	// symmetry object
	Symmetry symm;
	
	// enable window
	symm.Enable(true);
	symm.ShowPlane(true);
	symm.Type(0);
	
	// mirror set to "false"
	symm.SetMirror(false,0);
	symm.SetMirror(false,1);
	symm.SetMirror(false,2);
	symm.CoordSystemXYZ(0);
	symm.StartPoint(Vec3(0.00,0.00,0.00));
	
	// clear the retopo
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives tool
	PrimitivesTool tool;
	
	// go to the retopo primitives
	coat.room("Retopo");
	cmd("$RTP_PRIM");
	
	// get the retopo sphere
	rGeoSphere rsphere22;
	rsphere22 = tool.rsphere();
	
	// init parameters
	rsphere22.useDiameter(false);
	rsphere22.Position(Vec3(0.00,0.00,0.00));
	rsphere22.Radius(10.00);
	rsphere22.SubDivision(8);
	
	// go to the retopo primitives
	cmd("$RTP_PRIM");
	Step(1);
	
	// get the retopo box
	rGeoBox rbox23;
	rbox23 = tool.rbox();
	
	// init parameters
	rbox23.AxisX(Vec3(1.00,0.00,0.00));
	rbox23.AxisY(Vec3(0.00,1.00,0.00));
	rbox23.AxisZ(Vec3(0.00,0.00,1.00));
	rbox23.useFillet(false);
	rbox23.useUniform(true);
	rbox23.Position(Vec3(0.00,0.00,0.00));
	rbox23.SideA(30.00);
	rbox23.SideB(30.00);
	rbox23.SideC(30.00);
	rbox23.DivX(1);
	rbox23.DivY(1);
	rbox23.DivZ(1);
	rbox23.AverageDivision(12);
	
	// change the sides
	rbox23.SideA(134.45);
	rbox23.SideB(134.45);
	rbox23.SideC(134.45);
	
	// add the box in the scene
	tool.Apply();
	
	// go to the retopo primitives
	cmd("$RTP_PRIM");
	Step(1);

	// go to a sphere	
	tool.SetPrim("rsphere");
	rsphere22.Position(Vec3(-83.80,182.77,57.41));
	rsphere22.Radius(60.48);
	
	// add the sphere in the scene
	tool.Apply();
	
	// go to the retopo primitives
	cmd("$RTP_PRIM");
	Step(1);
	
	// get the retopo cylinder
	rGeoCylinder rcylinder24;
	rcylinder24 = tool.rcylinder();
	
	// init parameters ( diameter, fillet, removecaps )
	rcylinder24.useDiameter(false);
	rcylinder24.useFillet(false);
	rcylinder24.RemoveCaps(false);
	
	// uniform subdivision
	rcylinder24.useUniform(true);
	
	// positions (start,end points)
	rcylinder24.startPoint(Vec3(0.00,0.00,0.00));
	rcylinder24.endPoint(Vec3(0.00,20.00,0.00));
	
	// height, radius, topRadius, bottomRadius
	rcylinder24.height(20.00);
	rcylinder24.radius(0.00);
	rcylinder24.topRadius(10.00);
	rcylinder24.bottomRadius(10.00);
	
	// scales
	rcylinder24.scalex(1.00);
	rcylinder24.scaley(1.00);
	
	// custom divisions
	rcylinder24.DivY(1);
	rcylinder24.DivX(1);
	rcylinder24.DivZ(3);
	
	// average division
	rcylinder24.AverageDivision(16);
	
	// set the new positions
	rcylinder24.startPoint(Vec3(60.54,134.45,61.37));
	rcylinder24.endPoint(Vec3(60.54,163.89,61.37));
	
	// set the new height, radius, topRadius, bottomRadius 
	rcylinder24.height(129.44);
	rcylinder24.radius(47.91);
	rcylinder24.topRadius(47.91);
	rcylinder24.bottomRadius(47.91);
	rcylinder24.DivZ(7);
	
	// add the cylinder in the scene
	tool.Apply();
	
	// go to the retopo primitives
	cmd("$RTP_PRIM");
	Step(1);
	
	// get the retopo cone
	rGeoCone rcone25;
	rcone25 = tool.rcone();
	
	// init parameters (diameters, fillet)
	rcone25.useDiameter(false);
	rcone25.useFillet(false);
	
	// uniform subdivision
	rcone25.useUniform(true);
	
	// set the positions
	rcone25.startPoint(Vec3(0.00,0.00,0.00));
	rcone25.endPoint(Vec3(0.00,20.00,0.00));
	
	// height & radius
	rcone25.height(20.00);
	rcone25.bottomRadius(10.00);
	
	// scales
	rcone25.scalex(1.00);
	rcone25.scaley(1.00);
	
	// custom divisions
	rcone25.DivY(1);
	rcone25.DivX(1);
	rcone25.DivZ(3);
	
	// average division
	rcone25.AverageDivision(12);
	
	// change the parameters
	rcone25.startPoint(Vec3(-65.00,134.45,-69.28));
	rcone25.endPoint(Vec3(-65.00,198.84,-69.28));
	rcone25.height(164.39);
	rcone25.bottomRadius(47.91);
	rcone25.DivY(4);
	rcone25.DivZ(4);
	
	// add the cone in the scene
	tool.Apply();
	
	// go to the retopo primitives
	cmd("$RTP_PRIM");
	Step(1);
	
	// get the retopo spiral
	rGeoSpiral rspiral31;
	rspiral31 = tool.rspiral();
	
	// init parameters (axises)
	rspiral31.AxisX(Vec3(0.00,0.00,-1.00));
	rspiral31.AxisY(Vec3(1.00,-0.00,0.00));
	rspiral31.AxisZ(Vec3(0.00,-1.00,-0.00));
	
	// uniform subdivision
	rspiral31.useUniform(true);
	
	// positions
	rspiral31.Position(Vec3(0.00,0.00,0.00));
	rspiral31.Position2(Vec3(0.00,2.00,0.00));
	
	// sphere profile
	rspiral31.UseRectProfile(false);
	
	// outer diameter
	rspiral31.OuterDiameter(40.00);
	
	// pitch
	rspiral31.Pitch(20.00);
	
	// profile params (d & r)
	rspiral31.ProfileDiameter(10.00);
	rspiral31.ProfileRadiusRelation(0.50);
	
	// amount of turns
	rspiral31.Turns(10);
	
	// clockwise direction
	rspiral31.ClockWise(true);
	
	// custom subdivisions
	rspiral31.DivY(3);
	rspiral31.DivX(3);
	rspiral31.DivZ(3);
	
	// average division
	rspiral31.AverageDivision(3);
	
	// change the axises
	rspiral31.AxisX(Vec3(-1.00,-0.07,0.00));
	rspiral31.AxisY(Vec3(-0.07,1.00,0.00));
	rspiral31.AxisZ(Vec3(-0.00,0.00,-1.00));
	
	// set the new positions 
	rspiral31.Position(Vec3(-8.92,24.72,134.45));
	rspiral31.Position2(Vec3(-8.92,24.72,136.45));
	
	// set the new outer diameter
	rspiral31.OuterDiameter(120);
	
	// change the pitch
	rspiral31.Pitch(47.91);
	rspiral31.ProfileDiameter(30);
	
	// change the divisions
	rspiral31.DivY(8);
	rspiral31.DivZ(1);
	
	// change the average divisions
	rspiral31.AverageDivision(16);
	
	// add the spiral in the scene
	tool.Apply();
	
	// change locations (axes & positions) 
	rspiral31.AxisX(Vec3(-0.00,0.13,0.99));
	rspiral31.AxisY(Vec3(0.00,0.99,-0.13));
	rspiral31.AxisZ(Vec3(-1.00,0.00,-0.00));
	rspiral31.Position(Vec3(134.45,29.35,-0.27));
	rspiral31.Position2(Vec3(136.45,29.35,-0.27));
	
	// add the spiral in the scene
	tool.Apply();
}
