﻿void main() {
	Coat coat;
	// go to the retopo room 
	coat.room("Retopo");
	
	// clear room
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives room
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to the primitives
	cmd("$RTP_PRIM");
	
	// create the spiral
	rGeoSpiral rspiral31;
	rspiral31 = tool.rspiral();
	
	// init the axes
	rspiral31.AxisX(Vec3(0.00,-0.49,-0.87));
	rspiral31.AxisY(Vec3(1.00,0.00,0.00));
	rspiral31.AxisZ(Vec3(0.00,-0.87,0.49));
	
	// uniform is 'true'
	rspiral31.useUniform(true);
	
	// set the positions
	rspiral31.Position(Vec3(-140.16,0.00,69.79));
	rspiral31.Position2(Vec3(-140.16,1.75,68.82));
	
	// use a circle profile
	rspiral31.UseRectProfile(false);
	
	// set a outer diameter
	rspiral31.OuterDiameter(151.22);
	
	// set a pitch & profile diameter & radius relation
	rspiral31.Pitch(75.61);
	rspiral31.ProfileDiameter(37.81);
	rspiral31.ProfileRadiusRelation(0.50);
	
	// set the number of turns
	rspiral31.Turns(3);
	
	// clockwise = 'true'
	rspiral31.ClockWise(true);
	
	// set a number of divisions
	rspiral31.DivY(54);
	rspiral31.DivX(18);
	rspiral31.DivZ(8);
	rspiral31.AverageDivision(37);
	
	// add torus in the scene
	tool.Apply();
}
