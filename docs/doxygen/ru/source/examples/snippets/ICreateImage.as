﻿void main() {

    Coat coat;
	// перейти в скульпт комнату 
	coat.room("Sculpt");
	
	// воксельный объект
	Vox vox1;
	// очищаем сцену
	vox1.clearScene();
	vox1.rename("Volume3");

	// перейти к примитивам
	UI ui;
	ui("$SCULP_PRIM");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// создать объект изображение
	ImagePrim image12;
	image12 = tool.image();
	
	image12.AxisX(Vec3(0.80,0.00,-0.60));
	image12.AxisY(Vec3(0.00,1.00,0.00));
	image12.AxisZ(Vec3(0.60,0.00,0.80));
	
	// позиция и стороны
	image12.Position(Vec3(-257.19,0.00,282.29));
	image12.SideA(128.02);
	image12.SideB(128.02);
	image12.SideC(12.80);
	
	// вращение
	image12.ExtraRotation(0.00);
	// изгиб выключить
	image12.Bend(false);
	
	ImageMeshParam imparam1;
	
	// установить размер в сцену
	imparam1.SizeInScene(100.00);
	
	// установить верхнее изображение
	imparam1.Top("/images/Bracelet.png");
	imparam1.TopBump("/images/Bracelet.png");
	
	// ширина изображения
	imparam1.TopBottomWeight(0.70);
	
	imparam1.BasicThickness(8.00);
	imparam1.BumpThickness(8.00);
	
	// закрепить изображение
	image12.PickImage();
	
	// resize object
	image12.SideA(91.12);
	image12.SideB(91.12);
	image12.SideC(9.11);
	
	// добавить объект в сцену
	tool.Apply(0);
}
