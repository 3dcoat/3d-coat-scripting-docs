void main() {

    // подготавливаем сцену
    Vox  v;
    v.clearScene();

    // строим дерево v-слоёв
    v
      .appendToParent( "Head" )
      .appendToParent( "Body" )
        .append(         "Neck" )
        .appendToParent( "Left arm" )
        .appendToParent( "Right arm" )
        .appendToParent( "Heart" )
        .appendToParent( "Left leg" )
        .appendToParent( "Right leg" )

      // возвращаемся к слою "Head", переводим его
      // в "Режим поверхности" и аттачим новые
      .to( "Head" ).toSurface()
        .append( "Eye" )
        .appendToParent( "Lamps" )
      // Заметили как добавились последние?
      // Верно: `S`, не `V`.
    ;

    // удаляем первый (пустой) слой со сцены
    v.firstForRoot().remove();
}
