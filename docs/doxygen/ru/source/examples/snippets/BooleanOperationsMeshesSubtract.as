void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // создаём две сферы разного размера рядом так,
    // чтобы они перекрывали друг друга
    Builder builder;
    Mesh a = builder.sphere()
      .radius( 70 )
      .details( 0.1 )
      .build();
    Mesh b = builder.sphere()
      .radius( 40 )
      .position( Vec3( 30, 40, 50 ) )
      .details( 0.5 )
      .build();

    // вычитаем меши
    Mesh result = a - b;

    // размещаем полученный меш на сцене
    room += result;
}
