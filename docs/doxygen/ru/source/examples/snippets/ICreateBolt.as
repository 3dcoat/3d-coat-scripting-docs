﻿void main() {
    Coat coat;
	UI ui;
	// идем в комнату лепки
	coat.room("Sculpt");
	
	Vox vox1;
	// очищаем сцену
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// переходим к примитивам
	ui("$SCULP_PRIM");
	
	// создаем объект болта
	BoltPrim bolt18;
	bolt18 = tool.bolt();
	// устанавливает пользовательский режим
	bolt18.Custom();
	// позиция 
	bolt18.Position(Vec3(-336.02,-80.03,161.63));
	// головка болта с шестигранной головкой
	bolt18.HeadType(SHeadType::HEXA);
	// профиль треугольника
	bolt18.Profile(ProfileType::TRIANGLE);
	
	// переходим в поверхностный режим
	vox1.toSurface();
	
	// изменяем параметры оси
	bolt18.AxisX(Vec3(-0.01,-0.22,-0.97));
	bolt18.AxisY(Vec3(0.93,0.36,-0.09));
	bolt18.AxisZ(Vec3(0.37,-0.91,0.20));
	
	bolt18.Diameter(281.71);
	bolt18.Height(93.90);
	bolt18.BodyLength(281.71);
	bolt18.BodyDiameter(140.85);
	bolt18.ThreadLength(187.81);
	bolt18.Pitch(23.48);
	// уровень детализации
	bolt18.details(0.32);
	// добавляем в сцену
	tool.Apply(0);
}
