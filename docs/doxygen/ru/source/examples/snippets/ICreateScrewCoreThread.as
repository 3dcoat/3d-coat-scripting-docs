﻿void main() {

  	Coat coat;
	UI ui;
	// go to sculpt room
	coat.room("Sculpt");
	
	Vox vox1;
	// clear the scene
	vox1.clearScene();
	vox1.rename("Volume36");
	// go to surface mode
	vox1.toSurface();
	
	// prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	// create core thread object
	CoreThreadPrim coreThread16;
	coreThread16 = tool.corethread();
	// position
	coreThread16.Position(Vec3(-423.00,-51.63,202.36));
	// length
	coreThread16.Length(252.50);
	// core diameter
	coreThread16.Diameter(126.25);
	// thread length
	coreThread16.ThreadLength(168.33);
	// pitch
	coreThread16.Pitch(21.04);
	// round type 
	coreThread16.Profile(ProfileType::ROUND);
	// add in the scene
	tool.Apply(0);
}
