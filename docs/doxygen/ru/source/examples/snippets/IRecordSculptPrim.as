﻿// Файл, созданный после записи скрипта в скульпт-комнате.
void main() {
	
	Coat coat;
	UI ui;
	// перейти в скульпт комнату
	coat.room("Sculpt");
	
	Symmetry symm;
	
	// разрешить симметрию
	symm.Enable(true);
	symm.ShowPlane(true);
	symm.Type(0);
	
	// зеркальное отображение "ложь"
	symm.SetMirror(false,0);
	symm.SetMirror(false,1);
	symm.SetMirror(false,2);
	symm.CoordSystemXYZ(0);
	symm.StartPoint(Vec3(0.00,0.00,0.00));
	
	SculptRoom sculpt;
	
	// очистить комнату
	sculpt.clear();
	sculpt.toVoxel();
	
	// воксельный объект
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume5");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// перейти к примитивам
	coat.room("Sculpt");
	ui("$SCULP_PRIM");

	// создать начальную сферу
	SpherePrim sphere1;
	sphere1 = tool.sphere();
	
	// параметры сферы
	sphere1.useDiameter(false);
	sphere1.Position(Vec3(0.00,0.00,0.00));
	sphere1.Radius(15.00);
	
	// перйти в сурфейсный режим
	vox1.toSurface();
	ui("$SCULP_PRIM");
	
	// создать куб
	CubPrim cube2;
	cube2 = tool.cube();
	
	// параметры куба
	cube2.AxisX(Vec3(1.00,0.00,0.00));
	cube2.AxisY(Vec3(0.00,1.00,0.00));
	cube2.AxisZ(Vec3(0.00,0.00,1.00));
	cube2.useFillet(true);
	cube2.Position(Vec3(0.00,0.00,0.00));
	cube2.SideA(30.00);
	cube2.SideB(30.00);
	cube2.SideC(30.00);
	cube2.details(0.36);
	
	// изменить позицию и размеры сторон
	cube2.Position(Vec3(0.00,0.00,-191.27));
	cube2.SideA(434.41);
	cube2.SideB(12.88);
	cube2.SideC(896.92);
	
	// добавить куб в сцену
	tool.Apply(0);
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	Step(1);
	
	// создать цилиндр 
	CylinderPrim cylinder4;
	cylinder4 = tool.cylinder();
	
	// параметры цилиндра
	cylinder4.useDiameter(false);
	cylinder4.useFillet(false);
	cylinder4.useSector(false);
	cylinder4.startPoint(Vec3(0.00,0.00,0.00));
	cylinder4.endPoint(Vec3(0.00,20.00,0.00));
	cylinder4.height(20.00);
	cylinder4.radius(10.00);
	cylinder4.topRadius(10.00);
	cylinder4.bottomRadius(10.00);
	cylinder4.scalex(1.00);
	cylinder4.scaley(1.00);
	cylinder4.details(0.36);
	
	// изменить позицию
	cylinder4.startPoint(Vec3(153.52,-6.44,194.20));
	cylinder4.endPoint(Vec3(153.52,-126.74,194.20));
	
	// высота, радиус
	cylinder4.height(120.30);
	cylinder4.radius(31.52);
	cylinder4.topRadius(31.52);
	cylinder4.bottomRadius(31.52);
	
	// добавить цилиндр в сцену
	tool.Apply(0);
	
	// новая позиция
	cylinder4.startPoint(Vec3(-147.54,-6.44,191.79));
	cylinder4.endPoint(Vec3(-147.54,-126.74,191.79));
	
	// добавить цилиндр в сцену
	tool.Apply(0);
	
	// новая позиция
	cylinder4.startPoint(Vec3(150.98,-6.44,-577.69));
	cylinder4.endPoint(Vec3(150.98,-126.74,-577.69));
	
	// добавить цилиндр в сцену
	tool.Apply(0);
	
	// новая позиция
	cylinder4.startPoint(Vec3(-149.89,-6.44,-568.62));
	cylinder4.endPoint(Vec3(-149.89,-126.74,-568.62));
	
	// добавить цилиндр в сцену
	tool.Apply(0);
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// создать многогранник
	NGonPrim ngon8;
	ngon8 = tool.ngon();
	
	// инициализация параметров
	ngon8.useDiameter(false);
	ngon8.useFillet(false);
	ngon8.startPoint(Vec3(0.00,0.00,0.00));
	ngon8.endPoint(Vec3(0.00,20.00,0.00));
	ngon8.height(20.00);
	ngon8.radius(10.00);
	ngon8.topRadius(10.00);
	ngon8.bottomRadius(10.00);
	ngon8.Order(8);
	ngon8.scalex(1.00);
	ngon8.scaley(1.00);
	
	// уровень детализации
	ngon8.details(0.36);
	
	// изменить позицию
	ngon8.startPoint(Vec3(-75.14,6.44,99.46));
	ngon8.endPoint(Vec3(-75.14,356.44,99.46));
	// изменить радиус
	ngon8.radius(65.01);
	ngon8.topRadius(65.01);
	ngon8.bottomRadius(65.01);
	
	// добавить в сцену
	tool.Apply(0);
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	Step(1);
	
	// создать шестеренку 
	GearPrim gear9;
	gear9 = tool.gear();
	
	// инициализация параметров
	gear9.useDiameter(false);
	gear9.startPoint(Vec3(0.00,0.00,0.00));
	gear9.endPoint(Vec3(0.00,20.00,0.00));
	gear9.height(20.00);
	gear9.radius(10.00);
	gear9.topRadius(10.00);
	gear9.bottomRadius(10.00);
	gear9.Depth(0.20);
	gear9.Sharpness(0.50);
	gear9.Order(16);
	gear9.scalex(1.00);
	gear9.scaley(1.00);
	gear9.details(0.36);
	
	// изменить позицию
	gear9.startPoint(Vec3(-64.67,6.44,-410.43));
	gear9.endPoint(Vec3(-64.67,300.44,-410.43));
	
	// изменить радиус
	gear9.radius(55.01);
	gear9.topRadius(55.01);
	gear9.bottomRadius(55.01);
	
	// добавить в сцену
	tool.Apply(0);
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	Step(1);
	
	// создать шрифт
	Font font;
	font.Name("Arial");
	font.Size(384);
	font.Weight(0);
	font.Style(0);
	font.OutPrecision(0);
	font.ClipPrecision(0);
	font.CharSet(0);
	font.Quality(0);
	font.Family(0);
	
	// создать текстовый объект
	TextPrim text11;
	text11 = tool.text();
	
	// текст
	text11.SetText("Hello world");
	text11.AxisX(Vec3(1.00,0.00,0.00));
	text11.AxisY(Vec3(0.00,1.00,0.00));
	text11.AxisZ(Vec3(0.00,0.00,1.00));
	text11.Position(Vec3(0.00,0.00,0.00));
	
	// размеры
	text11.SideA(50.00);
	text11.SideB(50.00);
	text11.SideC(5.00);
	text11.ExtraRotation(0.00);
	
	// кривизна выкл.
	text11.Bend(false);
	
	// оси
	text11.AxisX(Vec3(-0.03,0.00,-1.00));
	text11.AxisY(Vec3(0.00,1.00,0.00));
	text11.AxisZ(Vec3(1.00,-0.00,-0.03));
	
	// позиция и размеры сторон
	text11.Position(Vec3(72.97,68.81,-207.88));
	text11.SideA(380.69);
	text11.SideB(380.69);
	text11.SideC(38.07);
	
	// добавить текст в сцену
	tool.Apply(0);
}

