﻿void main() {
	Coat coat;
	UI ui;
	//перейти в скульпт-комнату
	coat.room("Sculpt");
	
	// подготовка сцены
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//очистить комнату
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume58");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// создать объект капсула
	CapsulePrim capsule16;
	capsule16 = tool.capsule();
	// начальные параметры
	capsule16.useDiameter(false);
	
	// позиция
	capsule16.startPoint(Vec3(-502.04,-138.54,138.58));
	capsule16.endPoint(Vec3(-502.04,31.45,138.58));
	
	capsule16.height(169.98);
	capsule16.radius(41.13);
	capsule16.topRadius(41.13);
	capsule16.bottomRadius(41.13);
	
	// масштаб
	capsule16.scalex(1.00);
	capsule16.Cap1(1.00);
	capsule16.scaley(1.00);
	capsule16.Cap2(1.00);
	capsule16.scalex(1.00);
	capsule16.Cap1(1.00);
	capsule16.scaley(1.00);
	capsule16.Cap2(1.00);
	
	// перейти в поверхностный режим
	vox1.to("Volume58");
	vox1.toSurface();
	
	// изменить параметры
	capsule16.startPoint(Vec3(-457.88,-216.68,227.52));
	capsule16.endPoint(Vec3(-583.86,-116.64,64.04));
	capsule16.height(229.36);
	capsule16.radius(55.49);
	capsule16.topRadius(55.49);
	capsule16.bottomRadius(55.49);
	capsule16.details(0.84);
	
	// добавить в сцену
	tool.Apply(0);
}
