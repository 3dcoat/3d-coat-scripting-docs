﻿void main() {
	Coat coat;
	UI ui;
	// перейти в скульпт комнату 
	coat.room("Sculpt");
	
	//подготовить сцену
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//очистить сцену
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// создать объект многогранник
	NGonPrim ngon8;
	ngon8 = tool.ngon();
	
	// позиция
	ngon8.startPoint(Vec3(238.79,-48.69,340.52));
	ngon8.endPoint(Vec3(238.79,118.53,340.52));
	
	// высота & радиус
	ngon8.height(167.22);
	ngon8.radius(68.07);
	
	// кол-во сторон
	ngon8.Order(6);
	
	// добавить в сцену
	tool.Apply(0);
}
