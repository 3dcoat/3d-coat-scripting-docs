void main() {

    // подготавливаем сцену
    SculptRoom room;
    // очищаем скульпт-комнату и переводим её в сёрфейсный режим
    room.clear().toSurface();

    // строим сферу
    Builder builder;
    Mesh mesh = builder.sphere()
      .radius( 70 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // добавляем сферу на сцену
    room += mesh;
}
