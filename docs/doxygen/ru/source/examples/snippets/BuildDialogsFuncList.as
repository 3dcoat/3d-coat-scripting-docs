// Show model dialog with text identified ID. The ID is used to take
// translation from language .xml or just may be shown directly
// if translation is not found. 
void ModalDialog(string &in ID,string &in Caption);

// Show dialog with text identified ID and two buttons - Ok and Cancel.
// Returns true if OK pressed, false if Cancel pressed.
// The ID is used to take translation from language .xml or just may be
// shown directly if translation is not found.
bool ModalDialogOkCancel(string &in ID,string &in Caption);

// Show dialog with text identified ID and two buttons - Yes and No.
// Returns true if OK pressed, false if Cancel pressed.
// ID used to take translation from language xml or just may be shown
// directly if translation not found.
// Important! All dialogs may expose list of parameters and you are
// able to change value of any local or global variable via dialogs.
// See below a set of functions that allows to control additional parameters.
bool ModalDialogYesNo(string &in ID,string &in Caption);

// Each function that adds control passes variable name or other ID.
// Of course names of variables are are not always obvious to end-user.
// So you may translate in on normal language and make correspondence
// between ID (name of variable or any other ID in UI) and displayed text. 
void AddTranslation(string& ID, string& Text);

// Add slider for floating variable.  Most of functions have
// VariableName parameter. It is the name of variable (local or global)
// you want to edit in the dialog.  Look example a bit later.
// Min, Max – range if the value. Variable should be declared as
// float VariableName; in global or local scope. 
void AddFloatSlider(string &in VariableName,float Min,float Max);

// Add slider for integer variable. Variable should be declared as
// int VariableName; in global or local scope. 
void AddIntSlider(string &in VariableName,int Min,int Max);

// Add input box for floating variable. 
void AddFloatInput(string &in VariableName,bool EmptyName);

// Add input box for integer variable. 
void AddIntInput(string &in VariableName,bool EmptyName);

// Add input box for string variable. 
void AddStringInput(string &in VariableName,bool EmptyName);

// Insert the text among the list of variables. 
void AddTextField(string &in TextID,int Align);//1-center,0-left

// Insert delimiter.
void AddDelimiter();

// Insert button that will call function 
void AddButton(string &in FuncName);

// Place next controls in `nc` columns.
void Columns(int nc);
// Example:
    Columns(2);
    AddButton("Function1");
    AddButton("Function2");
// In this case 2 buttons will be aligned horizontally in the dialog. 

// Add checkbox. BoolVarRef shoud refer existing boolean variable
// declared as bool VariableName; 
void AddCheckBox(string &in BoolVarRef);

// Add droplist with several cases. Case index will be stored
// in `IntVarRef` variable. CaseList is list of possible values
// for droplist, delimiters are `,;|`.
void AddDroplist(string &in IntVarRef,string &in CaseList);
// Example
    Int Case=0;
    ...
    AddDroplist("Case","Case1,Case2,Case3");

// \see StopUICondition()
void UICondition(string& fn);

// That functions allow to control appearance of UI elements.
void StopUICondition();
// Example explains all:
    bool ShowSlider1and2;
    float Slider1;
    float Slider2;
    float Slider3;
    bool CheckUI(){
        return ShowSlider1and2;
    }
    void main(){
        AddCheckBox("ShowSlider1and2");
        UICondition("CheckUI"); // function CheckUI should return true if elements below should be visible
        AddFloatSlider("Slider1",0,123);
        AddFloatSlider("Slider2",0,123);
        StopUICondition(); // This function ends scope of previous UICondition
        AddFloatSlider("Slider3",0,123); // This slider is always visible
        ModalDialogOkCancel("","");
    }

// This function should be called before you call any of modal dialogs
// function if you want to press button number ButtonIndex
// (first button is 1, second is 2).
void PressInNextModalDialogs(int ButtonIndex);
// For example, if you have Yes and No buttons and you call
// PressInNextDialogs(1) before showing dialog then Yes will be
// pressed automatically.
// You should call PressInNextDialogs(-1) to stop automatical
// pressing of buttons.

// When you call any modal dialog the execution of the script will be
// stopped until user press Ok or other button in the dialog.
// Thus you are loosing control over dialog execution. If you want to do
// some action in the dialog, change some field you need to setup routine.
// That will be called constantly when dialog will be active.
// The routine may change fields in dialog, press buttons, and do
// other things.
// You may get the name of a current dialog name using next function.
void SetModalDialogCallback(string &in name);

// Remove all dialog callbacks.
void RemoveModalDialogCallbacks();

// Show non-modal message on screen that will be hidden after some
// period of time will pass (Time, sec).
void ShowFloatingMessage(string &in ID,float Time);

// Returns true if you are in dialog now, also it retuns text and
// caption in a current dialog to identify it.
bool GetCurrentDialog(string &out ID,string &out Caption);

// Get index of button pressed in last dialog. Usually OK=1, Cancel=2.
int GetLastButtonIndex();
