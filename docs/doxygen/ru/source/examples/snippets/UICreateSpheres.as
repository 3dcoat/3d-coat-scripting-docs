// доступ к отладке
Debug dbg;
DebugLog log = dbg.log();


void main() {

    log.clear();

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // построим сферу
    UI ui;
    ui.toSculptRoom();
    // обращаемся, по возможности, к названиям, которые видим на экране
    // для получения значений полей в кавычках, обращайтесь к ним
    // по внутреннему имени; для его получения, нажми MClick + RClick
    // на интересующем тебя поле
    // говорим `*Apply` вместо `Apply`, чтобы 3DCoat понял наше намерение,
    // а именно: *нажать* на кнопку
    ui( "Primitives" )( "Create sphere" )
      ( "$SpherePrim::%$px4[62]", 0 )
      ( "$SpherePrim::%$py5[62]", 0 )
      ( "*Apply" );

    // и ещё одну сферу рядом
    ui( "$SpherePrim::%$px4[62]", 50 )
      ( "*Apply" );

    // и ещё одну выше
    ui( "$SpherePrim::%$py5[62]", 50 )
      ( "*Apply" );
}
