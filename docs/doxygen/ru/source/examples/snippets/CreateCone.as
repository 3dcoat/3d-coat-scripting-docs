void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим конус
    Builder builder;
    Mesh mesh = builder.cone()
      .radius( 50 )
      .height( 120 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем конус на сцене
    room += mesh;
}
