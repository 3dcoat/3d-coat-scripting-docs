﻿void main() {

    Coat coat;
	UI ui;
	// перейти в скульпт комнату 
	coat.room("Sculpt");
	
	// подготовить сцену
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	// очищаем сцену
	Vox vox1;
	vox1.rename("Volume36");
	vox1.toSurface();
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// создать объект плавающие примитивы
	FreeFormPrim ff32;
	ff32 = tool.freeform("Blob:Blob2x2");
	
	ff32.Transform(false);
	ff32.SymmetryX(false);
	ff32.SymmetryY(false);
	ff32.SymmetryZ(false);
	
	// инициировать точки
	ff32.SetPoint(0,-51.67,-51.67,51.67);
	ff32.SetPoint(1,-51.67,51.67,51.67);
	ff32.SetPoint(2,51.67,51.67,51.67);
	ff32.SetPoint(3,51.67,-51.67,51.67);
	ff32.SetPoint(4,-51.67,-51.67,-51.67);
	ff32.SetPoint(5,-51.67,51.67,-51.67);
	ff32.SetPoint(6,51.67,51.67,-51.67);
	ff32.SetPoint(7,51.67,-51.67,-51.67);
	
	// 0 - индекс  of "Blob2x2"
	ff32.SubPrim(0);
	// уровень детализации
	ff32.details(0.26);
	// изменить позицию точки
	ff32.SetPoint(0,-97.98,-184.13,169.57);
	ff32.SetPoint(1,-137.95,-23.70,192.67);
	ff32.SetPoint(2,-75.81,-118.47,333.79);
	ff32.SetPoint(3,25.56,-210.63,211.70);
	ff32.SetPoint(4,-113.92,116.51,-214.19);
	ff32.SetPoint(5,121.35,-253.16,-74.83);
	ff32.SetPoint(6,172.05,6.77,-149.14);
	ff32.SetPoint(7,125.49,-172.24,-62.55);
	
	// добавить в сцену
	tool.Apply(0);
}
