﻿void main() { 
	Coat coat;
	// перейти в ретопо
	coat.room("Retopo");
	
	// очистить сцену
	RetopoRoom retopo;
	retopo.clear();
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// перейти к примитивам
	cmd("$RTP_PRIM");
	
	// создать бокс
	rGeoBox rbox23;
	rbox23 = tool.rbox();
	// оси
	rbox23.AxisX(Vec3(1.00,0.00,0.00));
	rbox23.AxisY(Vec3(0.00,1.00,0.00));
	rbox23.AxisZ(Vec3(0.00,0.00,1.00));
	rbox23.useUniform(true);
	// позиция
	rbox23.Position(Vec3(-226.48,32.41,0.00));
	// установить стороны 
	rbox23.SideA(64.79);
	rbox23.SideB(64.79);
	rbox23.SideC(64.79);
	
	// инициализация разбиений
	rbox23.DivX(1);
	rbox23.DivY(1);
	rbox23.DivZ(1);
	rbox23.AverageDivision(1);
	
	// позиция
	rbox23.Position(Vec3(-179.87,32.41,0.00));
	
	// стороны
	rbox23.SideA(85.72);
	rbox23.SideB(85.72);
	rbox23.SideC(85.72);
	
	// разделения
	rbox23.DivX(7);
	rbox23.DivY(7);
	rbox23.DivZ(7);
	rbox23.AverageDivision(7);
	
	// добавить бокс в сцену
	tool.Apply();
}
