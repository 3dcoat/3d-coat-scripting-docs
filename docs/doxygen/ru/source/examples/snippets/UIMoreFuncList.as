// То же, что `ui( string )`, но здесь обращаться к UI можно
// только по внутреннему имени.
DEPRECATED bool cmd( string );

// Пропустить `n` прорисовок интерфейса 3DCoat.
void Step( int n );

// Подождать указанный в миллисекундах интервал и продолжить
// выполнение скрипта.
void Wait( int ms );

// Создать меню, которое будет запускать скрипт.
// Скрипт будет скопирован в папку `Scripts/ExtraMenuItems`.
// \param path Path to script which will run.
void InstallToMenu( string path, string itemName );

// Check if element exists in UI. ID has same meaning as in cmd.
DEPRECATED bool FieldExists(string &in ID);
// Example – how to get to know amount of lights in Render room?
    void main(){
        int n=0;
        do{
            string s="$ExtraLight::Color["+formatInt(n,"l")+"]";
            if(FieldExists(s)){
                n++;
            }else break;
        }while(true);
        string s="Amount of lights = "+formatInt(n,"l");
        ModalDialog(s,"");
    }

// Get bool field from UI. ID has same meaning as in cmd.
DEPRECATED bool GetBoolField(string &in ID);

// Set value of the boolean field in UI. ID has same meaning as in cmd.
// Returns true if set value successfuly.
DEPRECATED bool SetBoolField(string &in ID,bool val);

// Get color field from UI as integer value. ID has same meaning as in cmd.
DEPRECATED int GetColorField(string &in ID);

// Set value of the color field in UI. ID has same meaning as in cmd.
// Returns true if set value successfully. 
// Example of colors – 0xFF0000 – red, 0x00FF00 – green, 0x0000FF - blue.
DEPRECATED bool SetColorField(string &in ID,bool val);

// Get value of slider in UI. ID has same meaning as in cmd.
DEPRECATED float GetSliderValue(string &in ID);

// Set value of slider in UI. ID has same meaning as in cmd.
// Returns true if set value successfuly.
DEPRECATED bool SetSliderValue(string &in ID,float val);

// Get value of edit box in UI. ID has same meaning as in cmd.
// Returns 0 if field not found.
DEPRECATED float GetEditBoxValue(string &in ID);

// Get value of edit box in UI. ID has same meaning as in cmd.
// Returns true if field found.
DEPRECATED bool GetEditBoxValue(string &out ID,string &out value);

// Set value of edit box in UI. ID has same meaning as in cmd.
// Returns true if field found.
DEPRECATED bool SetEditBoxValue(string &in ID,string &out val);

DEPRECATED bool SetEditBoxValue(string &in ID,float val);

DEPRECATED void SubstituteInputText(const string & val);

// Substitute string or value to the next input  text dialog.
// You need this command if there is button that triggers input
// dialog to enter some text or value.
DEPRECATED void SubstituteInputText(float val);
// Example - transform tool, ScaleY button. Code to scane object
// twice along Y-axis:
    SubstituteInputText(200.0);
    cmd("$CubPrim::ScaleY");
    Step(1);    
