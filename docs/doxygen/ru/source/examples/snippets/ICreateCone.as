﻿void main() {
	Coat coat;
	UI ui;
	// идем в скульпт-комнату
	coat.room("Sculpt");
	
	//подготовка сцены
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//очищаем сцену
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	// переход к примитивам
	ui("$SCULP_PRIM");
	
	//создать конус
	ConePrim cone5;
	cone5 = tool.cone();
	
	// инициализация
	cone5.startPoint(Vec3(0.00,0.00,0.00));
	cone5.endPoint(Vec3(0.00,20.00,0.00));
	cone5.height(20.00);
	cone5.bottomRadius(10.00);
	
	// изменить параметры
	cone5.useFillet(true);
	cone5.filletRadiusRelation(0.20);
	cone5.startPoint(Vec3(-285.69,-58.62,257.72));
	cone5.endPoint(Vec3(-305.78,108.83,232.01));
	cone5.height(170.60);
	cone5.bottomRadius(74.79);
	
	// добавить в сцену
	tool.Apply(0);
}
