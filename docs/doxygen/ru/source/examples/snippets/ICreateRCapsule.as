﻿void main() {
	Coat coat;
	
	// go to retopo room
	coat.room("Retopo");
	
	// clear
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives room
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to primitives
	cmd("$RTP_PRIM");
	
	// create the capsule
	rGeoCapsule rcapsule28;
	rcapsule28 = tool.rcapsule();
	
	// init parameters
	rcapsule28.useDiameter(false);
	rcapsule28.useUniform(true);
	
	// positions
	rcapsule28.startPoint(Vec3(0.00,0.00,0.00));
	rcapsule28.endPoint(Vec3(0.00,20.00,0.00));
	
	// height
	rcapsule28.height(20.00);
	
	// radius
	rcapsule28.radius(10.00);
	rcapsule28.topRadius(10.00);
	rcapsule28.bottomRadius(10.00);
	
	// scales & caps
	rcapsule28.scalex(1.00);
	rcapsule28.Cap1(1.00);
	rcapsule28.scaley(1.00);
	rcapsule28.Cap2(1.00);
	
	// divisions
	rcapsule28.DivY(2);
	rcapsule28.DivZ(6);
	rcapsule28.DivX(1);
	rcapsule28.AverageDivision(3);
	
	// sets the start & end positions
	rcapsule28.startPoint(Vec3(-27.64,-37.33,156.80));
	rcapsule28.endPoint(Vec3(-54.29,101.93,125.67));
	
	// sets the height
	rcapsule28.height(145.17);
	
	// sets the radius
	rcapsule28.radius(66.11);
	rcapsule28.topRadius(66.11);
	rcapsule28.bottomRadius(66.11);
	
	// sets the count of divisions
	rcapsule28.DivY(14);
	rcapsule28.DivZ(39);
	rcapsule28.DivX(10);
	rcapsule28.AverageDivision(21);
	
	// add in the scene
	tool.Apply();
}
