void main() {

/// [Example]
    SculptRoom  sculpt;

    // grab from named layer
    // if absent then take current
    Mesh  a( "Layer A" );
    auto  transform = a.tools().transform();

    // get & set & add to scene
    const int  N = 4;
    for ( int i = 0; i < N; ++i ) {
        const Vec3    translation = transform.position() + Vec3::rand( -48, 48 );
        const Angles  angles = transform.rotation() + Angles::rand( -48, 48 );
        transform
          .position( translation )
          .rotation( angles )
          ();
        sculpt += a;
    }
/// [Example]
}
