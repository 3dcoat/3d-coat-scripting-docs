﻿// access to debugging
Debug dbg;
DebugLog log = dbg.log();

void main() {

    Coat coat;
	log.clear();
	
	// go to sculpt room
	coat.room("Sculpt");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");
	
	// voxel object
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume0");
 	
	//prepare the primitives tool
	PrimitivesTool tool;
	
   	//create sphere obj
	SpherePrim sphere = tool.sphere();
	//create cube obj
	CubPrim cube = tool.cube();
	//create cylinder obj
	CylinderPrim cylinder = tool.cylinder();
	//create cone obj
	ConePrim cone = tool.cone();
	//create torus obj
	TorusPrim torus = tool.torus();
	
	float timeDelay = 2000; //delay 2 sec
	
	//goto to sphere 
	tool.SetPrim("sphere");
	
	sphere.Radius(50.0);
	sphere.Position(Vec3(-400,-50,0));
	
	// add sphere in the scene
	tool.Apply(0);

	//goto cylinder 
	tool.SetPrim("cylinder");
	
	cylinder.radius(50.0);
	cylinder.startPoint(Vec3(-300,0,0));
	cylinder.endPoint(Vec3(-300,100,0));
	
	// add cylinder in the scene
	tool.Apply(0);

	// goto cube
	tool.SetPrim("cube");
	Wait(timeDelay);
		
	// goto cone
	tool.SetPrim("cone");
	
	// sets parameters
	cone.startPoint(Vec3(-500,0,0));
	cone.endPoint(Vec3(-500,100,0));
	cone.radius(50);
	
	// add cone in the scene
	tool.Apply(0);
}
