﻿void main() {
	Coat coat;
	// go to retopo room
	coat.room("Retopo");
	
	// clear retopo 
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitive tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to primitives
	cmd("$RTP_PRIM");
	
	// create sphere 
	rGeoSphere rsphere22;
	rsphere22 = tool.rsphere();
	
	//init parameters
	rsphere22.useDiameter(false);
	rsphere22.Position(Vec3(-273.63,0.00,0.00));
	rsphere22.Radius(10.00);
	rsphere22.SubDivision(0);
	
	// set positions
	rsphere22.Position(Vec3(-252.48,80.03,0.00));
	// set a radius
	rsphere22.Radius(120.15);
	// set a subvisision
	rsphere22.SubDivision(5);
	
	// add sphere in the scene
	tool.Apply();
}
