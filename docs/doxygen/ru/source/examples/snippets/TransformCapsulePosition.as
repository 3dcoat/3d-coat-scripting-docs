void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим капсулу
    Builder builder;
    Mesh mesh = builder.capsule()
      .startPosition( Vec3( 0 ) )
      .endPosition( Vec3( 40, 50, 60 ) )
      .startRadius( 30 )
      .endRadius( 50 )
      .details( 0.1 )
      .build();

    // меняем позицию капсулы
    mesh.tools().transform()
      .position( Vec3( 100, 50, 200 ) )
      .run();
      
    // размещаем капсулу на сцене
    room += mesh;
}
