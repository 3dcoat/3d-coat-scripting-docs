﻿void main() {
	Coat coat;
	UI ui;
	// идем в скульпт-комнату
	coat.room("Sculpt");
	
	Vox vox1;
	//очищаем сцену
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	// переход к примитивам
	ui("$SCULP_PRIM");
	// создаем объект головки болта
	BoltHeadPrim bolthead17;
	bolthead17 = tool.bolthead();
	// начальная позиция
	bolthead17.Position(Vec3(-366.07,0.00,196.40));
	// шестнадцатеричный тип
	bolthead17.HeadType(SHeadType::HEXA);
	// отключаем прорезь
	bolthead17.UseSlit(false);
	bolthead17.Diameter(165.80);
	bolthead17.Height(55.27);
	// разрешаем прорезь
	bolthead17.UseSlit(true);
	// размеры щели
	bolthead17.SlitWidth(8.07);
	bolthead17.SlitLength(120.39);
	bolthead17.SlitHeight(5.00);
	// разрез
	bolthead17.SlitType(SSlitType::SLOT);
	// добавляем в сцену
	tool.Apply(0);
}
