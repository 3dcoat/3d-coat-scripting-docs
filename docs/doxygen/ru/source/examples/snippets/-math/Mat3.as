// This is simple template of 3D-Coat's script. Script language is very close to c++ and based on angelscript.
// You will usually see it if you used Scripts->Create new script
// Read general syntax description at http://angelcode.com/angelscript/sdk/docs/manual/doc_script.html and
// http://angelcode.com/angelscript/sdk/docs/manual/doc_understanding_as.html
// But if you know c/c++ it will be easy to write scripts. For beginning you may just use mostly one command 
// cmd("Some UI command");
// With this command you may execute any item in UI. It is easy to get UI item command - hover required comamnd in UI and press MMB and RMB simultaneously. 
// Corresponding command will be copied to clipboard. You may read about other commands in scripting manual.

void main() {

    // create an object (variant)
    Mat3 q;
    q.set( 0, 0, 100 );
    q.set( 1, 0, 110 );
    q.set( 2, 0,  q.get( 1, 0 ) + 10.2 );
    ModalDialog(
        "| " + q.get( 0, 0 ) + "  " + q.get( 0, 1 ) + "  " + q.get( 0, 2 ) + " |\n"
        "| " + q.get( 1, 0 ) + "  " + q.get( 1, 1 ) + "  " + q.get( 1, 2 ) + " |\n"
        "| " + q.get( 2, 0 ) + "  " + q.get( 2, 1 ) + "  " + q.get( 2, 2 ) + " |\n",
        "q" );

    // create an object (variant)
    Mat3 b(
        2, 0, 6,
        0, 3, 0,
        9, 0, 1 );
    const Mat3 recoveryB = b;
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + " |\n",
        "b" );

    // calculates a determinant of the matrix
    // @see http://mathworld.wolfram.com/Determinant.html
    ModalDialog( "" + b.determinant(), "b.determinant()" );

    // calculates a trace of the matrix
    // @see http://mathworld.wolfram.com/MatrixTrace.html
    ModalDialog( "" + b.trace(), "b.trace()" );

    // inverts the matrix
    b.invert();
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + " |\n",
        "b.invert()" );
    b = recoveryB;

    // orthonormalize the matrix
    // @see isOrthonormal()
    // @see http://reference.wolfram.com/language/Compatibility/tutorial/LinearAlgebra/Orthogonalization.html
    b.orthoNormalize();
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + " |\n",
        "b.orthoNormalize()" );
    b = recoveryB;

    // transpose the matrix
    // @see http://mathworld.wolfram.com/Transpose.html
    b.transpose();
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + " |\n",
        "b.transpose()" );
    b = recoveryB;
    
    // verify when the matrix is zero
    ModalDialog( b.isZero() ? "true" : "false", "b.isZero()" );
    Mat3 zero(
        0, 0, 0,
        0, 0, 0,
        0, 0, 0 );
    ModalDialog( zero.isZero() ? "true" : "false", "zero.isZero()" );

    // verify when the matrix is identity
    ModalDialog( b.isIdentity() ? "true" : "false", "b.isIdentity()" );
    Mat3 identity(
        1, 0, 0,
        0, 1, 0,
        0, 0, 1 );
    ModalDialog( identity.isIdentity() ? "true" : "false", "identity.isIdentity()" );

    // verify when the matrix is symmetric
    ModalDialog( b.isSymmetric() ? "true" : "false", "b.isSymmetric()" );
    Mat3 symmetric(
        1, 0, 0,
        0, 1, 0,
        0, 0, 1 );
    ModalDialog( symmetric.isSymmetric() ? "true" : "false", "symmetric.isSymmetric()" );

    // verify when the matrix is orthonormal
    // @see orthoNormalize()
    ModalDialog( b.isOrthonormal() ? "true" : "false", "b.isOrthonormal()" );
    Mat3 orthonormal = b;
    orthonormal.orthoNormalize();
    ModalDialog( orthonormal.isOrthonormal() ? "true" : "false", "orthonormal.isOrthonormal()" );

    // convert to matrix 4x4
    Mat4 b4 = b.toMat4();
    ModalDialog(
        "| " + b4.get( 0, 0 ) + "  " + b4.get( 0, 1 ) + "  " + b4.get( 0, 2 ) + "  " + b4.get( 0, 3 ) + " |\n"
        "| " + b4.get( 1, 0 ) + "  " + b4.get( 1, 1 ) + "  " + b4.get( 1, 2 ) + "  " + b4.get( 1, 3 ) + " |\n"
        "| " + b4.get( 2, 0 ) + "  " + b4.get( 2, 1 ) + "  " + b4.get( 2, 2 ) + "  " + b4.get( 2, 3 ) + " |\n"
        "| " + b4.get( 3, 0 ) + "  " + b4.get( 3, 1 ) + "  " + b4.get( 3, 2 ) + "  " + b4.get( 3, 3 ) + " |\n",
        "b.toMat4()" );
    
    // extract a forward-vector
    Vec3 forward = b.toForward();
    ModalDialog( forward.x + " " + forward.y + " " + forward.z, "b.toForward()" );
    
    // extract a right-vector
    Vec3 right = b.toRight();
    ModalDialog( right.x + " " + right.y + " " + right.z, "b.toRight()" );
    
    // extract an up-vector
    Vec3 up = b.toUp();
    ModalDialog( up.x + " " + up.y + " " + up.z, "b.toUp()" );
}
