// This is simple template of 3D-Coat's script. Script language is very close to c++ and based on angelscript.
// You will usually see it if you used Scripts->Create new script
// Read general syntax description at http://angelcode.com/angelscript/sdk/docs/manual/doc_script.html and
// http://angelcode.com/angelscript/sdk/docs/manual/doc_understanding_as.html
// But if you know c/c++ it will be easy to write scripts. For beginning you may just use mostly one command 
// cmd("Some UI command");
// With this command you may execute any item in UI. It is easy to get UI item command - hover required comamnd in UI and press MMB and RMB simultaneously. 
// Corresponding command will be copied to clipboard. You may read about other commands in scripting manual.

void main() {

    // create an object (variant)
    Vec3 a;
    a.x = 10.1;
    a.y = -50.5;
    a.z = 90.9;
    const Vec3 recoveryA = a;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a" );

    // create an object (variant)
    Vec3 b( 9, 49, 89 );
    ModalDialog( b.x + " " + b.y + " " + b.z, "b" );

    // create an object (variant)
    Vec3 c( 1000 );
    ModalDialog( c.x + " " + c.y + " " + c.z, "c" );

    // assign 'b' to 'c'
    c = b;
    ModalDialog( c.x + " " + c.y + " " + c.z, "c = b" );

    // some arithmetic operations
    a += b;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a += b" );
    a -= b;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a -= b" );

    a *= b;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a *= b" );
    a /= b;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a /= b" );

    a *= 1.2;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a *= 1.2" );
    a /= 1.2;
    ModalDialog( a.x + " " + a.y + " " + a.z, "a /= 1.2" );

    Vec3 d = a + b;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a + b" );
    d = a - b;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a - b" );
    d = a * b;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a * b" );
    d = a * 500;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a * 500" );
    d = a / b;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a / b" );
    d = a / 500;
    ModalDialog( d.x + " " + d.y + " " + d.z, "a / 500" );

    // equitations between vectors
    ModalDialog( (a == a) ? "true" : "false", "a == a" );
    ModalDialog( (a != a) ? "true" : "false", "a != a" );
    ModalDialog( (a == b) ? "true" : "false", "a == b" );
    ModalDialog( (a != b) ? "true" : "false", "a != b" );

    // calculates a distance between vectors
    ModalDialog( " " + a.distanceSquared( b ), "a.distanceSquared( b )" );
    ModalDialog( " " + a.distance( b ), "a.distance( b )" );

    // calculates a dot between vectors
    ModalDialog( " " + a.dot( b ), "a.dot( b )" );

    // calculates a cross between vectors
    Vec3 abCross = a.cross( b );
    ModalDialog( abCross.x + " " + abCross.y + " " + abCross.z, "a.cross( b )" );

    // calculates an Euclidean length of the vector
    ModalDialog( " " + a.lengthSquared(), "a.lengthSquared()" );
    ModalDialog( " " + a.length(), "a.length()" );
    
    // calculates a Manhattan length of the vector
    // @see https://en.wikipedia.org/wiki/Taxicab_geometry
    ModalDialog( " " + a.lengthManhattan(), "a.lengthManhattan()" );

    // normalize the vector and verify it
    // @see safeNormalize()
    ModalDialog( b.x + " " + b.y + " " + b.z, "b" );
    ModalDialog( b.isNormalized() ? "true" : "false", "b.isNormalized() before normalize" );
    float prevLength = b.normalize();
    ModalDialog( b.x + " " + b.y + " " + b.z, "b" );
    ModalDialog( b.isNormalized() ? "true" : "false", "b.isNormalized() after normalize" );
    ModalDialog( " " + prevLength, "previous length of 'b' after b.normalize()" );

    // safe normalize the vector
    // @see normalize()
    float nsPrevLength = a.normalizeSafe( Vec3( 1, 0, 0 ) );
    ModalDialog( " " + nsPrevLength, "previous length of 'a' after a.normalizeSafe( Vec3( 1, 0, 0 ) )" );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.normalizeSafe( Vec3( 1, 0, 0 ) )" );
    a = recoveryA;
    
    // fix normals
    ModalDialog( a.fixDegenerateNormal() ? "true" : "false", "a.fixDegenerateNormal()" );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.fixDegenerateNormal()" );
    a = recoveryA;

    // yet fix normals
    ModalDialog( a.fixDenormals() ? "true" : "false", "a.fixDenormals()" );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.fixDenormals()" );
    a = recoveryA;

    // round values of the vector to integers
    a.round();
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.round()" );
    a = recoveryA;

    // set values of the vector to absolute
    a.abs();
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.abs()" );
    a = recoveryA;

    // calculates an angle between vectors
    ModalDialog( " " + a.angle( b ), "a.angle( b )" );
    
    // linear interpolation
    // @see https://en.wikipedia.org/wiki/Linear_interpolation
    Vec3 lerp = a.lerp( b, 0.1 );
    ModalDialog( lerp.x + " " + lerp.y + " " + lerp.z, "a.lerp( b, 0.1 )" );

    // linear interpolation for a middle point
    // @lerp()
    Vec3 lerp05 = a.lerp05( b );
    ModalDialog( lerp05.x + " " + lerp05.y + " " + lerp05.z, "a.lerp05( b )" );

    // spherical linear interpolation
    // @see https://en.wikipedia.org/wiki/Slerp
    Vec3 slerp = a.slerp( b, 0.1 );
    ModalDialog( slerp.x + " " + slerp.y + " " + slerp.z, "a.slerp( b, 0.1 )" );

    // get a max value from the vectors
    Vec3 max = a.max( b );
    ModalDialog( max.x + " " + max.y + " " + max.z, "a.max( b )" );

    // get a min value from the vectors
    Vec3 min = a.min( b );
    ModalDialog( min.x + " " + min.y + " " + min.z, "a.min( b )" );

    // reflects the vector 'a' by normal 'b'
    Vec3 reflect = a.reflect( b );
    ModalDialog( reflect.x + " " + reflect.y + " " + reflect.z, "a.reflect( b )" );

    // refracts the vector 'a' by normal 'b' and 'eta'
     Vec3 refract = a.refract( b, 0.7 );
    ModalDialog( refract.x + " " + refract.y + " " + refract.z, "a.refract( b, 0.7 )" );

    // projection of the vector 'a' to the vector 'b'
    Vec3 project = a.project( b );
    ModalDialog( project.x + " " + project.y + " " + project.z, "a.project( b )" );

    // perpendicular to the vector
    Vec3 perpendicular = a.perpendicular();
    ModalDialog( perpendicular.x + " " + perpendicular.y + " " + perpendicular.z, "a.perpendicular()" );

    // clamp values of the vector to diapason [min; max]
    Vec3 p( 110, -30, 2500 );
    p.clamp( Vec3( 0, -10, 1000 ), Vec3( 100, 10, 2000 ) );
    ModalDialog( p.x + " " + p.y + " " + p.z,
        "p.clamp( Vec3( 0, -10, 1000 ), Vec3( 100, 10, 2000 ) )" );

    // truncate the vector by ‘length’
    a.truncate( 3.5 );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.truncate( 3.5 )" );
    a = recoveryA;

    // verify when the vector is zero
    ModalDialog( c.isZero() ? "true" : "false", "c.isZero()" );
    c = Vec3( 0 );
    ModalDialog( c.isZero() ? "true" : "false", "c.isZero()" );

    // apply a transform matrix to the vector
    ModalDialog( a.x + " " + a.y + " " + a.z, "a" );
    Vec3 scaling( 2, 5, 6 );
    Vec3 translation( -200, -50, 600 );
    Mat4 ma( scaling, translation );
    ModalDialog(
        "| " + ma.get( 0, 0 ) + "  " + ma.get( 0, 1 ) + "  " + ma.get( 0, 2 ) + "  " + ma.get( 0, 3 ) + " |\n"
        "| " + ma.get( 1, 0 ) + "  " + ma.get( 1, 1 ) + "  " + ma.get( 1, 2 ) + "  " + ma.get( 1, 3 ) + " |\n"
        "| " + ma.get( 2, 0 ) + "  " + ma.get( 2, 1 ) + "  " + ma.get( 2, 2 ) + "  " + ma.get( 2, 3 ) + " |\n"
        "| " + ma.get( 3, 0 ) + "  " + ma.get( 3, 1 ) + "  " + ma.get( 3, 2 ) + "  " + ma.get( 3, 3 ) + " |\n",
        "ma" );
    a.transformCoordinate( ma );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.transformCoordinate( ma )" );
    a = recoveryA;

    // calculates a normal for the vector
    Vec3 toNormal = b.toNormal();
    ModalDialog( toNormal.x + " " + toNormal.y + " " + toNormal.z, "b.toNormal()" );

    // rotates the vector by quaternion
    Quat quat( 0.2, 0.3, 0.7, 1.0 );
    a.rotate( quat );
    ModalDialog( a.x + " " + a.y + " " + a.z, "a.rotate( quat )" );
    a = recoveryA;
    
    // convert the vector to an object 'Angles'
    Angles angles = a.toAngles();
    ModalDialog( angles.pitch + " " + angles.yaw + " " + angles.roll, "a.toAngles()" );
}
