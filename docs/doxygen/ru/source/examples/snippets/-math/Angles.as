void main() {

    // create an object (variant)
    Angles a;
    a.pitch( 181 );
    a.yaw( -40.5 );
    a.roll( 365 );
    const Angles recoveryA = a;
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a" );

    // create an object (variant)
    Angles b( 10, 90, 270 );
    ModalDialog( b.pitch() + " " + b.yaw() + " " + b.roll(), "b" );

    // assign 'b' to 'c'
    Angles c = b;
    ModalDialog( c.pitch() + " " + c.yaw() + " " + c.roll(), "c = b" );

    // some arithmetic operations
    a += b;
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a += b" );
    a = recoveryA;
    
    a -= b;
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a -= b" );
    a = recoveryA;

    a *= 1.2;
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a *= 1.2" );
    a = recoveryA;
    
    a /= 1.2;
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a /= 1.2" );
    a = recoveryA;

    Angles d = a + b;
    ModalDialog( d.pitch() + " " + d.yaw() + " " + d.roll(), "a + b" );
    d = a - b;
    ModalDialog( d.pitch() + " " + d.yaw() + " " + d.roll(), "a - b" );
    d = a * 100;
    ModalDialog( d.pitch() + " " + d.yaw() + " " + d.roll(), "a * 100" );
    d = a / 100;
    ModalDialog( d.pitch() + " " + d.yaw() + " " + d.roll(), "a / 100" );

    // equitations between objects
    ModalDialog( (a == a) ? "true" : "false", "a == a" );
    ModalDialog( (a != a) ? "true" : "false", "a != a" );
    ModalDialog( (a == b) ? "true" : "false", "a == b" );
    ModalDialog( (a != b) ? "true" : "false", "a != b" );

    // normalizations angles to 360 degree
    a.normalize360();
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a.normalize360()" );
    a = recoveryA;
    
    // normalizations angles to 180 degree
    a.normalize180();
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a.normalize180()" );
    a = recoveryA;

    // rounds angles to integers
    a.round();
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a.round()" );
    a = recoveryA;

    // linear interpolation between angles
    Angles lerp = a.lerp( b, 0.1 );
    ModalDialog( lerp.pitch() + " " + lerp.yaw() + " " + lerp.roll(), "a.lerp( b, 0.1 )" );

    // Clamp angles to diapason [min; max].
    const Angles min( 200, -100, 300 );
    const Angles max( 360, 0, 360 );
    a.clamp( min, max );
    ModalDialog( a.pitch() + " " + a.yaw() + " " + a.roll(), "a.clamp( min, max )" );
    a = recoveryA;
}
