// This is simple template of 3D-Coat's script. Script language is very close to c++ and based on angelscript.
// You will usually see it if you used Scripts->Create new script
// Read general syntax description at http://angelcode.com/angelscript/sdk/docs/manual/doc_script.html and
// http://angelcode.com/angelscript/sdk/docs/manual/doc_understanding_as.html
// But if you know c/c++ it will be easy to write scripts. For beginning you may just use mostly one command 
// cmd("Some UI command");
// With this command you may execute any item in UI. It is easy to get UI item command - hover required comamnd in UI and press MMB and RMB simultaneously. 
// Corresponding command will be copied to clipboard. You may read about other commands in scripting manual.

void main() {

    // create an object (variant)
    Mat4 q;
    q.set( 0, 0, 100 );
    q.set( 1, 0, 110 );
    q.set( 2, 0,  q.get( 1, 0 ) + 10.2 );
    ModalDialog(
        "| " + q.get( 0, 0 ) + "  " + q.get( 0, 1 ) + "  " + q.get( 0, 2 ) + "  " + q.get( 0, 3 ) + " |\n"
        "| " + q.get( 1, 0 ) + "  " + q.get( 1, 1 ) + "  " + q.get( 1, 2 ) + "  " + q.get( 1, 3 ) + " |\n"
        "| " + q.get( 2, 0 ) + "  " + q.get( 2, 1 ) + "  " + q.get( 2, 2 ) + "  " + q.get( 2, 3 ) + " |\n"
        "| " + q.get( 3, 0 ) + "  " + q.get( 3, 1 ) + "  " + q.get( 3, 2 ) + "  " + q.get( 3, 3 ) + " |\n",
        "q" );

    // create an object (variant)
    // rotates through 120 degree about the axis x = y = z
    const Mat3 rotation(
        0, 0, 1,
        1, 0, 0,
        0, 1, 0 );
    const Vec3 translation( -200, -50, 600 );
    Mat4 aa( rotation, translation );
    ModalDialog(
        "| " + aa.get( 0, 0 ) + "  " + aa.get( 0, 1 ) + "  " + aa.get( 0, 2 ) + "  " + aa.get( 0, 3 ) + " |\n"
        "| " + aa.get( 1, 0 ) + "  " + aa.get( 1, 1 ) + "  " + aa.get( 1, 2 ) + "  " + aa.get( 1, 3 ) + " |\n"
        "| " + aa.get( 2, 0 ) + "  " + aa.get( 2, 1 ) + "  " + aa.get( 2, 2 ) + "  " + aa.get( 2, 3 ) + " |\n"
        "| " + aa.get( 3, 0 ) + "  " + aa.get( 3, 1 ) + "  " + aa.get( 3, 2 ) + "  " + aa.get( 3, 3 ) + " |\n",
        "aa" );

    // create an object (variant)
    const Vec3 scaling( 2, 5, 6 );
    Mat4 a( scaling, translation );
    ModalDialog(
        "| " + a.get( 0, 0 ) + "  " + a.get( 0, 1 ) + "  " + a.get( 0, 2 ) + "  " + a.get( 0, 3 ) + " |\n"
        "| " + a.get( 1, 0 ) + "  " + a.get( 1, 1 ) + "  " + a.get( 1, 2 ) + "  " + a.get( 1, 3 ) + " |\n"
        "| " + a.get( 2, 0 ) + "  " + a.get( 2, 1 ) + "  " + a.get( 2, 2 ) + "  " + a.get( 2, 3 ) + " |\n"
        "| " + a.get( 3, 0 ) + "  " + a.get( 3, 1 ) + "  " + a.get( 3, 2 ) + "  " + a.get( 3, 3 ) + " |\n",
        "a" );

    // create an object (variant)
    Mat4 b(
        2, 0, 0,  1,
        0, 3, 0,  2,
        0, 0, 1, -1,
        0, 0, 0,  1 );
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + "  " + b.get( 0, 3 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + "  " + b.get( 1, 3 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + "  " + b.get( 2, 3 ) + " |\n"
        "| " + b.get( 3, 0 ) + "  " + b.get( 3, 1 ) + "  " + b.get( 3, 2 ) + "  " + b.get( 3, 3 ) + " |\n",
        "b" );
    const Mat4 recoveryB = b;

    // create an object (variant)
    Mat4 translationXYZ( "translation", 11, 22, 33 );
    ModalDialog(
        "| " + translationXYZ.get( 0, 0 ) + "  " + translationXYZ.get( 0, 1 ) + "  " + translationXYZ.get( 0, 2 ) + "  " + translationXYZ.get( 0, 3 ) + " |\n"
        "| " + translationXYZ.get( 1, 0 ) + "  " + translationXYZ.get( 1, 1 ) + "  " + translationXYZ.get( 1, 2 ) + "  " + translationXYZ.get( 1, 3 ) + " |\n"
        "| " + translationXYZ.get( 2, 0 ) + "  " + translationXYZ.get( 2, 1 ) + "  " + translationXYZ.get( 2, 2 ) + "  " + translationXYZ.get( 2, 3 ) + " |\n"
        "| " + translationXYZ.get( 3, 0 ) + "  " + translationXYZ.get( 3, 1 ) + "  " + translationXYZ.get( 3, 2 ) + "  " + translationXYZ.get( 3, 3 ) + " |\n",
        "translationXYZ" );

    // create an object (variant)
    Mat4 translationVec3( "translation", translation );
    ModalDialog(
        "| " + translationVec3.get( 0, 0 ) + "  " + translationVec3.get( 0, 1 ) + "  " + translationVec3.get( 0, 2 ) + "  " + translationVec3.get( 0, 3 ) + " |\n"
        "| " + translationVec3.get( 1, 0 ) + "  " + translationVec3.get( 1, 1 ) + "  " + translationVec3.get( 1, 2 ) + "  " + translationVec3.get( 1, 3 ) + " |\n"
        "| " + translationVec3.get( 2, 0 ) + "  " + translationVec3.get( 2, 1 ) + "  " + translationVec3.get( 2, 2 ) + "  " + translationVec3.get( 2, 3 ) + " |\n"
        "| " + translationVec3.get( 3, 0 ) + "  " + translationVec3.get( 3, 1 ) + "  " + translationVec3.get( 3, 2 ) + "  " + translationVec3.get( 3, 3 ) + " |\n",
        "translationVec3" );

    // create an object (variant)
    Mat4 rotationXYZ( "rotation", 0.1, 0.3, 0.5 );
    ModalDialog(
        "| " + rotationXYZ.get( 0, 0 ) + "  " + rotationXYZ.get( 0, 1 ) + "  " + rotationXYZ.get( 0, 2 ) + "  " + rotationXYZ.get( 0, 3 ) + " |\n"
        "| " + rotationXYZ.get( 1, 0 ) + "  " + rotationXYZ.get( 1, 1 ) + "  " + rotationXYZ.get( 1, 2 ) + "  " + rotationXYZ.get( 1, 3 ) + " |\n"
        "| " + rotationXYZ.get( 2, 0 ) + "  " + rotationXYZ.get( 2, 1 ) + "  " + rotationXYZ.get( 2, 2 ) + "  " + rotationXYZ.get( 2, 3 ) + " |\n"
        "| " + rotationXYZ.get( 3, 0 ) + "  " + rotationXYZ.get( 3, 1 ) + "  " + rotationXYZ.get( 3, 2 ) + "  " + rotationXYZ.get( 3, 3 ) + " |\n",
        "rotationXYZ" );

    // create an object (variant)
    Mat4 rotationVec3( "rotation", Vec3( 0.1, 0.3, 0.5 ) );
    ModalDialog(
        "| " + rotationVec3.get( 0, 0 ) + "  " + rotationVec3.get( 0, 1 ) + "  " + rotationVec3.get( 0, 2 ) + "  " + rotationVec3.get( 0, 3 ) + " |\n"
        "| " + rotationVec3.get( 1, 0 ) + "  " + rotationVec3.get( 1, 1 ) + "  " + rotationVec3.get( 1, 2 ) + "  " + rotationVec3.get( 1, 3 ) + " |\n"
        "| " + rotationVec3.get( 2, 0 ) + "  " + rotationVec3.get( 2, 1 ) + "  " + rotationVec3.get( 2, 2 ) + "  " + rotationVec3.get( 2, 3 ) + " |\n"
        "| " + rotationVec3.get( 3, 0 ) + "  " + rotationVec3.get( 3, 1 ) + "  " + rotationVec3.get( 3, 2 ) + "  " + rotationVec3.get( 3, 3 ) + " |\n",
        "rotationVec3" );

    // create an object (variant)
    const Vec3 point( 100, 150, 500 );
    const Vec3 axis( 0, 1, 1 );
    Mat4 rotationAt( point, axis, 0.7 );
    ModalDialog(
        "| " + rotationAt.get( 0, 0 ) + "  " + rotationAt.get( 0, 1 ) + "  " + rotationAt.get( 0, 2 ) + "  " + rotationAt.get( 0, 3 ) + " |\n"
        "| " + rotationAt.get( 1, 0 ) + "  " + rotationAt.get( 1, 1 ) + "  " + rotationAt.get( 1, 2 ) + "  " + rotationAt.get( 1, 3 ) + " |\n"
        "| " + rotationAt.get( 2, 0 ) + "  " + rotationAt.get( 2, 1 ) + "  " + rotationAt.get( 2, 2 ) + "  " + rotationAt.get( 2, 3 ) + " |\n"
        "| " + rotationAt.get( 3, 0 ) + "  " + rotationAt.get( 3, 1 ) + "  " + rotationAt.get( 3, 2 ) + "  " + rotationAt.get( 3, 3 ) + " |\n",
        "rotationAt" );

    // create an object (variant)
    Mat4 scalingXYZ( "scaling", 10, 30, 50 );
    ModalDialog(
        "| " + scalingXYZ.get( 0, 0 ) + "  " + scalingXYZ.get( 0, 1 ) + "  " + scalingXYZ.get( 0, 2 ) + "  " + scalingXYZ.get( 0, 3 ) + " |\n"
        "| " + scalingXYZ.get( 1, 0 ) + "  " + scalingXYZ.get( 1, 1 ) + "  " + scalingXYZ.get( 1, 2 ) + "  " + scalingXYZ.get( 1, 3 ) + " |\n"
        "| " + scalingXYZ.get( 2, 0 ) + "  " + scalingXYZ.get( 2, 1 ) + "  " + scalingXYZ.get( 2, 2 ) + "  " + scalingXYZ.get( 2, 3 ) + " |\n"
        "| " + scalingXYZ.get( 3, 0 ) + "  " + scalingXYZ.get( 3, 1 ) + "  " + scalingXYZ.get( 3, 2 ) + "  " + scalingXYZ.get( 3, 3 ) + " |\n",
        "scalingXYZ" );

    // create an object (variant)
    Mat4 scalingVec3( "scaling", scaling );
    ModalDialog(
        "| " + scalingVec3.get( 0, 0 ) + "  " + scalingVec3.get( 0, 1 ) + "  " + scalingVec3.get( 0, 2 ) + "  " + scalingVec3.get( 0, 3 ) + " |\n"
        "| " + scalingVec3.get( 1, 0 ) + "  " + scalingVec3.get( 1, 1 ) + "  " + scalingVec3.get( 1, 2 ) + "  " + scalingVec3.get( 1, 3 ) + " |\n"
        "| " + scalingVec3.get( 2, 0 ) + "  " + scalingVec3.get( 2, 1 ) + "  " + scalingVec3.get( 2, 2 ) + "  " + scalingVec3.get( 2, 3 ) + " |\n"
        "| " + scalingVec3.get( 3, 0 ) + "  " + scalingVec3.get( 3, 1 ) + "  " + scalingVec3.get( 3, 2 ) + "  " + scalingVec3.get( 3, 3 ) + " |\n",
        "scalingVec3" );

    // calculates a determinant of the matrix
    // @see http://mathworld.wolfram.com/Determinant.html
    ModalDialog( "" + b.determinant(), "b.determinant()" );

    // calculates a trace of the matrix
    // @see http://mathworld.wolfram.com/MatrixTrace.html
    ModalDialog( "" + b.trace(), "b.trace()" );

    // inverts the matrix
    b.invert();
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + "  " + b.get( 0, 3 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + "  " + b.get( 1, 3 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + "  " + b.get( 2, 3 ) + " |\n"
        "| " + b.get( 3, 0 ) + "  " + b.get( 3, 1 ) + "  " + b.get( 3, 2 ) + "  " + b.get( 3, 3 ) + " |\n",
        "b.invert()" );

    // transpose the matrix
    // @see http://mathworld.wolfram.com/Transpose.html
    b.transpose();
    ModalDialog(
        "| " + b.get( 0, 0 ) + "  " + b.get( 0, 1 ) + "  " + b.get( 0, 2 ) + "  " + b.get( 0, 3 ) + " |\n"
        "| " + b.get( 1, 0 ) + "  " + b.get( 1, 1 ) + "  " + b.get( 1, 2 ) + "  " + b.get( 1, 3 ) + " |\n"
        "| " + b.get( 2, 0 ) + "  " + b.get( 2, 1 ) + "  " + b.get( 2, 2 ) + "  " + b.get( 2, 3 ) + " |\n"
        "| " + b.get( 3, 0 ) + "  " + b.get( 3, 1 ) + "  " + b.get( 3, 2 ) + "  " + b.get( 3, 3 ) + " |\n",
        "b.transpose()" );
    
    // verify when the matrix is zero
    ModalDialog( b.isZero() ? "true" : "false", "b.isZero()" );
    Mat4 zero(
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0 );
    ModalDialog( zero.isZero() ? "true" : "false", "zero.isZero()" );

    // verify when the matrix is identity
    ModalDialog( b.isIdentity() ? "true" : "false", "b.isIdentity()" );
    Mat4 identity(
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1 );
    ModalDialog( identity.isIdentity() ? "true" : "false", "identity.isIdentity()" );

    // harvest a scaling from the matrix
    Vec3 s = b.scaling();
    ModalDialog( s.x + " " + s.y + " " + s.z, "b.scaling()" );
    
    // harvest a translation from the matrix
    Vec3 t = b.translation();
    ModalDialog( t.x + " " + t.y + " " + t.z, "b.translation()" );
}
