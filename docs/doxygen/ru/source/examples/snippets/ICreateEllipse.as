﻿void main() {

	Coat coat;
	UI ui;
	// перейти в скульпт-комнату
	coat.room("Sculpt");

	//щчистить скульпт-комнату
	SculptRoom sculpt;
	sculpt.clear();
	sculpt.toVoxel();
	
	// получить воксельный объект
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume12");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// получить еллипс
	EllipsePrim ellipse3;
	ellipse3 = tool.ellipse();

	// инициализация параметров
	ellipse3.Position(Vec3(-307.18,61.64,380.09));
	ellipse3.SideA(40.82);
	ellipse3.SideB(40.89);
	ellipse3.SideC(108.31);
	ellipse3.AxisX(Vec3(1.00,-0.04,-0.01));
	ellipse3.AxisY(Vec3(0.04,0.99,0.15));
	ellipse3.AxisZ(Vec3(0.00,-0.15,0.99));
	
	// перейти в серфиксный режим
	vox1.toSurface();
	
	// установить новые стороны
	ellipse3.SideA(51.39);
	ellipse3.SideB(51.48);
	ellipse3.SideC(136.36);
	// установить уровень детализации
	ellipse3.details(0.61);
	// добавить в сцену
	tool.Apply(0);
}
