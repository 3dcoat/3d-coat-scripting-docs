﻿void main() {
	Coat coat;
	// go to sculpt room
	coat.room("Sculpt");
	
	// voxel object
	Vox vox1;
	//clear the scene
	vox1.clearScene();
	vox1.rename("Volume4");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");
	
	// prepare the primitives tool
	PrimitivesTool tool;
	
	// sphere object 	
	SpherePrim sphere1;
	//gets the sphere tool
	sphere1 = tool.sphere();
	
	// initializing parameters
	sphere1.Position(Vec3(0.00,0.00,0.00));
	sphere1.Radius(15.00);
	
	// go tо the surface mode
	vox1.toSurface();
	
	//Change the radius and position
	sphere1.Position(Vec3(-300.00,-12.00,369.07));
	sphere1.Radius(74.72);

  	// add the sphere in the scene 
	// Zero equals the binary operation "add"
	tool.Apply(0);

}
