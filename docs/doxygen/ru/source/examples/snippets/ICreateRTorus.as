﻿void main() {
	Coat coat;
	// go to the retopo room
	coat.room("Retopo");
	
	// clear the room
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to the primitives
	cmd("$RTP_PRIM");
	
	// create the torus
	rGeoTorus rtorus27;
	rtorus27 = tool.rtorus();
	
	// init axes
	rtorus27.AxisX(Vec3(1.00,0.00,0.00));
	rtorus27.AxisY(Vec3(0.00,1.00,0.00));
	rtorus27.AxisZ(Vec3(0.00,0.00,1.00));
	
	// set a uniform flag
	rtorus27.useUniform(true);
	
	// init position 
	rtorus27.Position(Vec3(0.00,0.00,0.00));
	
	// init sides
	rtorus27.SideA(30.00);
	rtorus27.SideB(30.00);
	rtorus27.SideC(30.00);
	
	// init radius relation
	rtorus27.RadiusRelation(0.50);
	
	// init a outer,inner and profile radius
	rtorus27.OuterRadius(30.00);
	rtorus27.InnerRadius(15.00);
	rtorus27.ProfileRadius(7.50);
	
	// set a default division
	rtorus27.AverageDivision(3);
	
	// set the axes
	rtorus27.AxisY(Vec3(0.00,0.28,-0.96));
	rtorus27.AxisZ(Vec3(0.00,0.96,0.28));
	
	// set the position
	rtorus27.Position(Vec3(-194.37,-15.59,52.99));
	
	// set a sides
	rtorus27.SideA(112.28);
	rtorus27.SideB(112.28);
	rtorus27.SideC(112.28);
	
	// set a outer, inner and profile radius
	rtorus27.OuterRadius(112.28);
	rtorus27.InnerRadius(56.14);
	rtorus27.ProfileRadius(28.07);
	
	// set an average division
	rtorus27.AverageDivision(27);
	
	// add torus in the scene
	tool.Apply();
}
