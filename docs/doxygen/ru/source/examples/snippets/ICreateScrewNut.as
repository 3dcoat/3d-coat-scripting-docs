﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create nut object
	NutPrim nut20;
	nut20 = tool.nut();
	
	// custom mode
	nut20.Custom();
	
	// init axises
	nut20.AxisX(Vec3(-0.59,0.36,-0.72));
	nut20.AxisY(Vec3(0.81,0.26,-0.53));
	nut20.AxisZ(Vec3(-0.00,-0.89,-0.45));
	
	// init position
	nut20.Position(Vec3(-30,0,0));
	
	//other parameters
	nut20.Name("hexa");
	nut20.Diameter(233.01);
	nut20.Height(116.51);
	nut20.HeadType(SNutType::HEXA);
	nut20.ThreadDiameter(155.34);
	nut20.Pitch(19.42);
	nut20.Profile(ProfileType::TRIANGLE);
	nut20.UseThread(true);
	
	// go to surface mode
	vox1.toSurface();
	
	// sets the detail level
	nut20.details(0.61);
	
	// add in the scene
	tool.Apply(0);
}
