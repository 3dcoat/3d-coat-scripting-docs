// доступ к отладке
Debug dbg;
DebugLog log = dbg.log();


void main() {

    // очищаем лог-файл
    log.clear();

    for( int i = 0; i < 1000; i++ ) {
        float x = GetMouseX();
        float y = GetMouseY();
        // печатаем в лог-файл координаты мыши каждые 50 мс
        log += "mouse coord " + x + " " + y;
        Wait( 50 );
    }
}
