﻿void main() {
	Coat coat;
	UI ui;
	// перейти в скульпт комнату 
	coat.room("Sculpt");
	
	// подготовить сцену
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	// очищаем сцену
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	// подготавливаем инструмент примитивов
	PrimitivesTool tool;
	
	// перейти к примитивам
	ui("$SCULP_PRIM");
	
	// создать шестеренку
	GearPrim gear9;
	gear9 = tool.gear();
	
	// позиция
	gear9.startPoint(Vec3(0.00,0.00,0.00));
	gear9.endPoint(Vec3(0.00,20.00,0.00));
	
	// инициализация параметров
	gear9.height(20.00);
	gear9.radius(10.00);
	gear9.Depth(0.10);
	gear9.Sharpness(0.5);
	gear9.Order(16);
	
	// изменить параметры
	gear9.startPoint(Vec3(285.31,-181.30,495.22));
	gear9.endPoint(Vec3(285.31,-33.71,478.45));
	gear9.height(148.55);
	gear9.radius(94.87);
	
	// добавить объект в сцену
	tool.Apply(0);
}
