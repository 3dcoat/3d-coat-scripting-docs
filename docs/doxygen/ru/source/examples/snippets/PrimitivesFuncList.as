// Reset additional transformation for low-level primitives.
DEPRECATED void ResetPrimTransform();

// Translate all further low-level primitives.
DEPRECATED void PrimTranslate(float dx,float dy,float dz);

// Scale all further low-level primitives using pivot point x,y,z.
DEPRECATED void PrimScaleAt(float x,float y,float z,float scalex,float scaley,float scalez);

// Rotate further primitives at xyz point around X-axis.
DEPRECATED void PrimRotateX(float x,float y,float z,float Angle);

// Rotate further primitives at xyz point around Y-axis.
DEPRECATED void PrimRotateY(float x,float y,float z,float Angle);

// Rotate further primitives at xyz point around Z-axis.
DEPRECATED void PrimRotateZ(float x,float y,float z,float Angle);

// Rotate further primitives around rode (x,y,z)->(xend,yend,zend)
// on given Angle.
DEPRECATED void PrimRotateAroundRode(float x,float y,float z,float xend,float yend,float zend,float Angle);

// Set special transform for further primitives.
// If you will apply primitive that is placed between points
// (0,-1,0) and (0,1,0) it will be actually applied as primitive
// stretched between points (x,y,z) and (xend,yend,zend).
DEPRECATED void PrimStretchBetweenPoints(float x,float y,float z,float xend,float yend,float zend);
// Example:
    void main(){
        ResetPrimTransform();
        // stretch between  (10,20,30) and (40,50,60)
        PrimStretchBetweenPoints(10,20,30,40,50,60);
        // starts from point (0,-1,0), radius=10, height=2
        cylinder(0,-1,0,10,10,2,0);
    }
// This code will create cylinder of radius 10 between
// points (10,20,30) and (40,50,60).

// Set additional density factor for low-level primitives.
// 1 means default density.
DEPRECATED void PrimDensity(float density);

// Store current transform for primitives as string to be kept
// for future usage.
DEPRECATED string GetPrimTransform();

// Restore current primitives transform from string that was
// previously kept using `GetPrimTransform()`.
DEPRECATED void SetPrimTransform(string& in M);
// Example:
SetPrimTransform(
    "1.0 2.0 3.0 1.0\n"
    "4.0 5.0 6.0 1.0\n"
    "7.0 8.0 9.0 1.0\n"
    "1.1 2.2 3.3 1.0" );

// Create sphere with radius `r`.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void sphere(float x,float y,float z,float r,int mode);

// Create ellipse.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void ellipse(float x,float y,float z,float rx,float ry,float rz,int mode);

// Create parallelepiped.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void cube(float x,float y,float z,float sizex,float sizey,float sizez,int mode);

// Create cylinder.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void cylinder(float x,float y,float z,float topradius,float bottomradius,float height,int mode);

// Create cone.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void cone(float x,float y,float z,float radius,float height,int mode);

// Create N-gon.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void ngon(float x,float y,float z,int sides,float topradius,float bottomradius,float height,int mode);

// Create tube.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void tube(float x,float y,float z,float topradius,float bottomradius,float height,float wallthickness,int mode);

// Create n-gonal tube.
// `x`, `y`, `z` means a center of figure.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void ngontube(float x,float y,float z,int sides,float topradius,float bottomradius,float height,float wallthickness,int mode);

// Creates capsule between points.
// Where `xstart`, `ystart`, `zstart` means coords of first half-sphere;
// `xend`, `yend`, `zend` means coords of second half-sphere.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void capsule(float xstart,float ystart,float zstart,float xend,float yend,float zend,float startradius,float endradius,int mode);
