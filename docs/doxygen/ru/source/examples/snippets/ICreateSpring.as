﻿void main() {

    Coat coat;
	UI ui;
	
	// go to sculpt room
	coat.room("Sculpt");
	
	SculptRoom sculpt;
	sculpt.toVoxel();
	
	//gets vox obj
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume4");
	
	PrimitivesTool tool;
	
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create the spiral 
	SpringPrim spring13;
	spring13 = tool.spring();
	
	// sets a position
	spring13.Position(Vec3(-318.50,0.00,316.36));
	spring13.Position2(Vec3(-318.50,2.00,316.40));
	
	// scale
	spring13.ScaleRadius(1.00);
	spring13.OuterDiameter(67.96);
	
	// sets the pitch
	spring13.Pitch(33.98);
	
	spring13.ProfileDiameter(16.99);
	spring13.ProfileRadiusRelation(0.50);
	// account turns 
	spring13.Turns(7);
	// spiral direction
	spring13.ClockWise(true);
	// add the spiral in the scene
	tool.Apply(0);
}
