﻿void main() {
	
	Coat coat;
	// go to retopo room
	coat.room("Retopo");
	// clear retopo
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to the primitives
	cmd("$RTP_PRIM");
	
	// create the cylinder
	rGeoCylinder rcylinder24;
	rcylinder24 = tool.rcylinder();
	
	// init flags
	rcylinder24.useDiameter(false);
	rcylinder24.useFillet(false);
	rcylinder24.RemoveCaps(false);
	rcylinder24.useUniform(true);
	
	// init positions 
	rcylinder24.startPoint(Vec3(-145.07,0.00,0.00));
	rcylinder24.endPoint(Vec3(-145.07,20.00,0.00));
	
	// init height,radius,topRadius,bottomRadius
	rcylinder24.height(20.00);
	rcylinder24.radius(10.00);
	rcylinder24.topRadius(10.00);
	rcylinder24.bottomRadius(10.00);
	
	// init divisions
	rcylinder24.DivY(3);
	rcylinder24.DivX(1);
	rcylinder24.DivZ(5);
	rcylinder24.AverageDivision(3);
	
	// change positions
	rcylinder24.startPoint(Vec3(-145.07,-101.38,0.00));
	rcylinder24.endPoint(Vec3(-145.07,45.82,0.00));
	
	// sets height
	rcylinder24.height(147.20);
	
	// sets radius,topRadius,bottomRadius
	rcylinder24.radius(72.82);
	rcylinder24.topRadius(72.82);
	rcylinder24.bottomRadius(72.82);

	// sets divisions
	rcylinder24.DivY(13);
	rcylinder24.DivX(3);
	rcylinder24.DivZ(20);
	rcylinder24.AverageDivision(12);
	
	// add in the scene
	tool.Apply();
}
