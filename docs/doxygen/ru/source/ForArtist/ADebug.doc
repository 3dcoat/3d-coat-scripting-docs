﻿/**

\page ADebug  🌀 Отладка скриптов

Скрипты в 3DCoat могут быть сложными: ведь они пишутся на AngelScript, а с этим языком уже
[связано](http://angelcode.com/angelscript/users.html)
довольно много больших и успешных проектов.

Поэтому, при написания своего скрипта, тебе может понадобиться знать
значения некоторых переменных в процессе выполнения.
И ты можешь это узнать.

Этот код включаем в начало своих скриптов и получаем возможность 
  - печатать в лог-файл (`log`)
  - рисовать на сцене (`drw`)

\include "snippets/DebugBlank.as"




Что можем печатать в лог?

\code
// значения конкретных переменных
float delta = 1.2345;
log += delta;
\endcode

\code
// результаты математических операций
log += pow( a, 3 ) * sqrt( 2 ) / 12;
\endcode

\code
// текст / заметку
log +=
    "This is a debug-note."
    " We do treat variables provisionally as constants.";
\endcode

\code
// все объявленные локальные переменные
log += dbg.vars();
// или (дальше, когда неск. вариантов, пишу без "или")
log += dbg.variables();
\endcode

\code
// все объявленные глобальные переменные
log += dbg.gvars();
log += dbg.globalVariables();
\endcode

\code
// текущий стек вызовов
log += dbg.callstack();
\endcode

\code
// меш
// структура меша появится в виде текста
Builder builder;
const Mesh a = builder.cylinder()
  .positionTop( Vec3( 0, 120, 0 ) )
  .positionBottom( Vec3( 0, 0, 0 ) )
  .radiusTop( 50 )
  .radiusBottom( 50 )
  .details( 0.1 )
  ();
log += a;
\endcode




Что можем отобразить во вьюпорт?

\code
// точку
// point( position, color )
drw.point( Vec3( 50, 0, 0 ) );
drw.point( Vec3( 50, 60, 0 ), Color( 0xFFAAFF33 ) );
\endcode

\code
// линию
// line( startPosition, endPosition, startColor, endColor )
drw.line( Vec3( 0, 0, 0 ), Vec3( 100, 50, 25 ) );
drw.line( Vec3( 0, 50, 0 ), Vec3( 0, 200, 25 ),
    Color( 0xFF33FF33 ) );
drw.line( Vec3( 100, 120, 0 ), Vec3( 100, 250, 25 ),
    Color( 0xFFAA0000 ), Color( 0xFF0000AA ) );
\endcode

\code
// круг
// circle( position, normal, radius, color )
drw.circle( Vec3( 0, 0, 0 ), Vec3( 100, 50, 25 ), 20 );
drw.circle( Vec3( 0, 50, 0 ), Vec3( 0, 200, 25 ), 50,
    Color( 0xFF55FFAA ) );
\endcode

\code
// сферу
// sphere( position, radius, color )
drw.sphere( Vec3( 0, 0, 0 ), 15 );
drw.sphere( Vec3( 0, 50, 0 ), 45,
    Color( 0x9955FFAA ) );
\endcode

\code
// треугольник
// triangle( A, B, C, colorA, colorB, colorC )
const Vec3  a( 0, 0, 0 );
const Vec3  b( 80, 30, 70 );
const Vec3  c( 10, 25, 100 ) );
drw.triangle( a, b, c );
drw.triangle( a, b, c,
    Color( 0x77AA0000 ) );
drw.triangle( a, b, c,
    Color( 0xFFAA0000 ), Color( 0xFF00AA00 ), Color( 0xFF0000AA ) );
\endcode

\code
// надпись, число
// note( position, text )
drw.note( Vec3( 0, 0, 0 ),  "Note A" );
drw.note( Vec3( 0, 30, 0 ), "Note B",
    Color( 0xFF33FF33 ) );
drw.note( Vec3( 0, 0, 20 ),  111 );
drw.note( Vec3( 0, 30, 20 ), 222.51,
    Color( 0xFF33FF33 ) );
\endcode




[Векторы](\ref coat::scripto::Vec3),
[матрицы](\ref coat::scripto::Mat4),
[боксы](\ref coat::scripto::AABB)
и пр. здесь не перечисляем: очевидно, что они печатаются / визуализируются просто: 

\code
log += МойОбъектКоторыйХочуНапечатать;
drw += МойОбъектКоторыйХочуНарисовать;
\endcode


Сложную визуализацию можно разбросать по слоям, чтобы отображать / скрывать их по отдельности:

\code
drw += "First";
// или `drw.layer( "First" );`
// теперь всё ниже рисуется в слое `First`
// ...
drw += "Second";
// а теперь - в слое `Second`
// ...
\endcode




\see \ref AHowRunOwnScript
\see [DebugDraw](\ref coat::scripto::DebugDraw)
\see [DebugLog](\ref coat::scripto::DebugLog)


**/
