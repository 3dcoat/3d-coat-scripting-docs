@echo off

for /f "delims=" %%i in ("%0") do set "currentpath=%%~dpi"
cd "%currentpath%"

call build.cmd "Doxy" > "build Doxy.log"
