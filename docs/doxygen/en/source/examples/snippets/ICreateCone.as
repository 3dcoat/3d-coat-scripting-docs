﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//prepare the scene
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create cone obj
	ConePrim cone5;
	cone5 = tool.cone();
	
	// init parameters
	cone5.startPoint(Vec3(0.00,0.00,0.00));
	cone5.endPoint(Vec3(0.00,20.00,0.00));
	cone5.height(20.00);
	cone5.bottomRadius(10.00);
	
	// change parameters
	cone5.useFillet(true);
	cone5.filletRadiusRelation(0.20);
	cone5.startPoint(Vec3(-285.69,-58.62,257.72));
	cone5.endPoint(Vec3(-305.78,108.83,232.01));
	cone5.height(170.60);
	cone5.bottomRadius(74.79);
	
	// add in the scene
	tool.Apply(0);
}
