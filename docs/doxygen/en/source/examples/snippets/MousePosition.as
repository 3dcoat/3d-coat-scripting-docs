// access to debugging
Debug dbg;
DebugLog log = dbg.log();


void main() {

    // clear log file
    log.clear();

    for( int i = 0; i < 1000; i++ ) {
        float x = GetMouseX();
        float y = GetMouseY();
        // print mouse coordinates each 50 ms to log file
        log += "mouse coordinate " + x + " " + y;
        Wait( 50 );
    }
}
