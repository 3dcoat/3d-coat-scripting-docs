void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a capsule
    Builder builder;
    Mesh mesh = builder.capsule()
      .startPosition( Vec3( 0 ) )
      .endPosition( Vec3( 40, 50, 60 ) )
      .startRadius( 30 )
      .endRadius( 50 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put a capsule into scene
    room += mesh;
}
