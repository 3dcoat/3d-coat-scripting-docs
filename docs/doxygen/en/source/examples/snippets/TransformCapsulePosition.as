void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build capsule
    Builder builder;
    Mesh mesh = builder.capsule()
      .startPosition( Vec3( 0 ) )
      .endPosition( Vec3( 40, 50, 60 ) )
      .startRadius( 30 )
      .endRadius( 50 )
      .details( 0.1 )
      .build();

    // change capsule position
    mesh.tools().transform()
      .position( Vec3( 100, 50, 200 ) )
      .run();
      
    // put capsule into scene
    room += mesh;
}
