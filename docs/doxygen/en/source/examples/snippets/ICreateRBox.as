﻿void main() { 
	Coat coat;
	// go to Retopo room
	coat.room("Retopo");
	
	// clear retopo
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare thw primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to the primitives
	cmd("$RTP_PRIM");
	
	// create the box (cube)
	rGeoBox rbox23;
	rbox23 = tool.rbox();
	// init axises
	rbox23.AxisX(Vec3(1.00,0.00,0.00));
	rbox23.AxisY(Vec3(0.00,1.00,0.00));
	rbox23.AxisZ(Vec3(0.00,0.00,1.00));
	rbox23.useUniform(true);
	// position
	rbox23.Position(Vec3(-226.48,32.41,0.00));
	// sets the side 
	rbox23.SideA(64.79);
	rbox23.SideB(64.79);
	rbox23.SideC(64.79);
	
	// init subdivisions
	rbox23.DivX(1);
	rbox23.DivY(1);
	rbox23.DivZ(1);
	rbox23.AverageDivision(1);
	
	// change position
	rbox23.Position(Vec3(-179.87,32.41,0.00));
	
	// change sides
	rbox23.SideA(85.72);
	rbox23.SideB(85.72);
	rbox23.SideC(85.72);
	
	// change subdivisions
	rbox23.DivX(7);
	rbox23.DivY(7);
	rbox23.DivZ(7);
	rbox23.AverageDivision(7);
	
	// add box in the scene
	tool.Apply();
}
