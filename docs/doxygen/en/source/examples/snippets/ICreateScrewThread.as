﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	Vox vox1;
	//clear the scene
	vox1.clearScene();
	vox1.rename("Volume3");
	// go to surface mode
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	// create thread object
 	ThreadPrim thread15;
	thread15 = tool.thread();
	// position
	thread15.Position(Vec3(-416.06,-69.18,174.47));
	//diameter
	thread15.Diameter(135.42);
	thread15.Pitch(22.57);
	thread15.Turns(10);
	// triangle profile
	thread15.Profile(ProfileType::TRIANGLE);
	thread15.ClockWise(true);
	thread15.StubHeight(4.51);
	// go to surface mode
	vox1.toSurface();
	// set detail level
	thread15.details(0.59);
	// add in the scene
	tool.Apply(0);
}
