﻿void main() {
	Coat coat;
	// go to retopo room
	coat.room("Retopo");
	// clear the room
	RetopoRoom retopo;
	retopo.clear();
	// prepare the primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	cmd("$RTP_PRIM");
	
	// create the cone
	rGeoCone rcone25;
	rcone25 = tool.rcone();
	
	// init flags
	rcone25.useDiameter(false);
	rcone25.useFillet(false);
	rcone25.useUniform(true);
	
	// init start & end position
	rcone25.startPoint(Vec3(0.00,0.00,0.00));
	rcone25.endPoint(Vec3(0.00,20.00,0.00));
	
	// height
	rcone25.height(20.00);
	rcone25.bottomRadius(10.00);
	
	// scales = 1
	rcone25.scalex(1.00);
	rcone25.scaley(1.00);
	
	// divisions
	rcone25.DivY(5);
	rcone25.DivX(1);
	rcone25.DivZ(4);
	rcone25.AverageDivision(3);
	
	// sets fillet to "true"
	rcone25.useFillet(true);
	
	// sets fillet radius 
	rcone25.filletRadiusRelation(0.34);
	rcone25.filletRadius(16.61);
	
	// sets the positions
	rcone25.startPoint(Vec3(-50.17,0.00,106.16));
	rcone25.endPoint(Vec3(-50.17,161.35,106.16));
	
	// sets the height
	rcone25.height(161.35);
	
	// sets the bottom radius
	rcone25.bottomRadius(75.93);
	
	// sets the count of divisions
	rcone25.DivY(19);
	rcone25.DivX(2);
	rcone25.DivZ(14);
	rcone25.AverageDivision(12);
	
	// add in the scene
	tool.Apply();
}
