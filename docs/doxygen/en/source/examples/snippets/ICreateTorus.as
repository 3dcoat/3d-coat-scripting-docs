﻿void main() {
	Coat coat;
	UI ui;
	
	// go to sculpt room
	coat.room("Sculpt");
	
	SculptRoom sculpt;
	sculpt.toVoxel();
	
	//gets vox obj
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume5");
	
	PrimitivesTool tool;
	
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create the torus
	TorusPrim torus14;
	torus14 = tool.torus();
	
	// init parameters
	torus14.Position(Vec3(0.00,0.00,0.00));
	torus14.SideA(30.00);
	torus14.SideB(30.00);
	torus14.SideC(30.00);
	torus14.RadiusRelation(0.50);
	torus14.OuterRadius(30.00);
	torus14.InnerRadius(15.00);
	torus14.ProfileRadius(7.50);
	torus14.AxisX(Vec3(1.00,0.05,-0.04));
	torus14.AxisY(Vec3(-0.05,1.00,0.05));
	torus14.AxisZ(Vec3(0.04,-0.05,1.00));
	
	// go to surface mode
	vox1.toSurface();
	
	// change parameters
	torus14.Position(Vec3(-525.58,-37.10,196.04));
	
	// sides
	torus14.SideA(102.64);
	torus14.SideB(102.64);
	torus14.SideC(102.64);
	
	// radius
	torus14.OuterRadius(102.64);
	torus14.InnerRadius(51.32);
	torus14.ProfileRadius(25.66);
	
	torus14.AxisX(Vec3(0.83,0.02,-0.56));
	torus14.AxisY(Vec3(-0.01,1.00,0.02));
	torus14.AxisZ(Vec3(0.56,-0.01,0.83));
	// details level
	torus14.details(0.81);
	// add the torus in the scene
	tool.Apply(0);
}
