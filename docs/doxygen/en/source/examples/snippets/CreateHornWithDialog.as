void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // default settings
    float angle = 10.0;
    int numberOfChunks = 20;

    // settings window
    AddTranslation( "numberOfChunks", "Number of chunks in horn" );
    AddIntInput( "numberOfChunks", true );
    AddTranslation( "angle", "Angle" );
    AddFloatInput( "angle", true );
    bool ok = ModalDialogOkCancel( "Enter the horn parameters", "Create horn" );
    if ( !ok ) {
        // end of script
        return;
    }

    // create a figure with set parameters
    ResetPrimTransform();
    PrimDensity( 0.3 );
    for ( int i = 0; i < numberOfChunks; i++ ) {
        capsule( -15, 0, 0, 15, 0, 0, 20, 20, 0 );
        PrimRotateY( 0, 0, 0, angle );
        PrimRotateX( 0, 0, 0, angle );
        PrimTranslate( 0, 15, 0 );
        PrimScaleAt( 10, 20, 30, 0.9, 0.9, 0.9 );
        ProgressBar( "Please wait", (i * 100) / numberOfChunks );
    }
}
