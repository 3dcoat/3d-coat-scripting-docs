// This is simple template of 3D-Coat's script. Script language is very close to c++ and based on angelscript.
// You will usually see it if you used Scripts->Create new script
// Read general syntax description at http://angelcode.com/angelscript/sdk/docs/manual/doc_script.html and
// http://angelcode.com/angelscript/sdk/docs/manual/doc_understanding_as.html
// But if you know c/c++ it will be easy to write scripts. For beginning you may just use mostly one command 
// cmd("Some UI command");
// With this command you may execute any item in UI. It is easy to get UI item command - hover required comamnd in UI and press MMB and RMB simultaneously. 
// Corresponding command will be copied to clipboard. You may read about other commands in scripting manual.

void main() {

    // create an object (variant)
    Quat a;
    a.x = 10;
    a.y = -15;
    a.z = 60;
    a.w = 1;
    const Quat recoveryA = a;
    ModalDialog( a.x + " " + a.y + " " + a.z + " " + a.w, "a" );

    // create an object (variant)
    Quat b( 220, 330, 770, 110 );
    const Quat recoveryB = b;
    ModalDialog( b.x + " " + b.y + " " + b.z + " " + b.w, "b" );

    // copy 'b' to 'assign'
    Quat assign = b;
    ModalDialog( assign.x + " " + assign.y + " " + assign.z + " " + assign.w, "assign = b" );

    // equitations between vectors
    ModalDialog( (a == b) ? "true" : "false", "a == b" );
    ModalDialog( (a != b) ? "true" : "false", "a != b" );
    ModalDialog( (a == a) ? "true" : "false", "a == a" );

    // compress the quaternions:
    // set ‘w’ to 0.0; change a sign of ‘x’, ‘y’, ‘z’ if ‘w’ was less zero
    b.compress();
    ModalDialog( b.x + " " + b.y + " " + b.z + " " + b.w, "b.compress()" );
    b = recoveryB;
    
    // recalculate ‘w’ by current ‘xyz’
    b.calcW();
    ModalDialog( b.x + " " + b.y + " " + b.z + " " + b.w, "b.calcW()" );
    b = recoveryB;
    
    // euclidean length of the quaternion
    ModalDialog( " " + a.lengthSquared(), "a.lengthSquared()" );
    ModalDialog( " " + a.length(), "a.length()" );

    // normalize the quaternion and verify it
    ModalDialog( a.isNormalized() ? "true" : "false", "a.isNormalized()" );
    a.normalize();
    ModalDialog( a.x + " " + a.y + " " + a.z + " " + a.w, "a.normalize()" );
    ModalDialog( a.isNormalized() ? "true" : "false", "a.isNormalized()" );
    a = recoveryA;

    // create a conjugate quaternion
    Quat r = a.conjugate();
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.conjugate()" );

    // calculates a dot-product of the quaternion
    // @see http://3dgep.com/understanding-quaternions/#Quaternion_Dot_Product
    ModalDialog( " " + a.dot( b ), "a.dot( b )" );

    // calculates the exponential
    r = a.exp();
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.exp()" );

    // invert the quaternion
    // @see http://3dgep.com/understanding-quaternions/#Quaternion_Inverse
    r = a.invert();
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.invert()" );

    // calculates a natural logarithm
    r = a.ln();
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.ln()" );

    // calculates a natural logarithm with different quaternions 'a' and 'b'
    r = a.lnDif( b );
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.lnDif( b )" );

    // linear interpolation
    r = a.lerp( b, 0.2, true );
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.lerp( b, 0.2, true )" );

    // spherical linear interpolation
    r = a.slerp( b, 0.2, true );
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w, "a.slerp( b, 0.2, true )" );

    // spherical quadrangle interpolation
    const Quat p1( 1, 3, 5, 7 );
    const Quat p2( 10, 30, 50, 70 );
    const Quat p3( 11, 33, 55, 77 );
    r = a.squad( p1, p2, p3, 0.2 );
    ModalDialog( r.x + " " + r.y + " " + r.z + " " + r.w,
        "a.squad( p1, p2, p3, 0.2 )" );

    // transform the quaternion to angles
    Angles angles = a.toAngles();
    ModalDialog( angles.pitch + " " + angles.yaw + " " + angles.roll, "a.toAngles()" );
}
