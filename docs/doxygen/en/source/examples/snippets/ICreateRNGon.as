﻿void main() {
	Coat coat;
	
	// go to retopo room
	coat.room("Retopo");
	
	// clear retopo room
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives room
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to primitives
	cmd("$RTP_PRIM");
	
	// create the n-gon
	rGeoNGon rngon30;
	rngon30 = tool.rngon();
	
	// init flags
	rngon30.useDiameter(false);
	rngon30.useFillet(true);
	rngon30.useUniform(false);
	
	// set a bevel to "true"
	rngon30.Bevel(1,true);
	rngon30.Bevel(2,true);
	
	// set a fillet(bevel) radiuses
	rngon30.filletRadiusRelation(0.02);
	rngon30.filletRadius(0.34);
	
	// set the positions 
	rngon30.startPoint(Vec3(-34.32,-54.38,124.74));
	rngon30.endPoint(Vec3(-18.28,125.10,124.74));
	
	// set height, radius, topRadius, bottomRadius
	rngon30.height(180.20);
	rngon30.radius(73.82);
	rngon30.topRadius(73.82);
	rngon30.bottomRadius(73.82);
	
	// set the number of edges (order)
	rngon30.Order(15);
	
	// number of segment for fillet
	rngon30.FilletSegs(1);
	
	// set the divisions
	rngon30.DivY(14);
	rngon30.DivX(5);
	rngon30.DivZ(4);
	
	// add in the scene
	tool.Apply();
}
