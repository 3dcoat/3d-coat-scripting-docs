void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a cone
    Builder builder;
    Mesh mesh = builder.cone()
      .radius( 50 )
      .height( 120 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put a cone into scene
    room += mesh;
}
