﻿void main() {
    Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	// create screw washer
	WasherPrim washer21;
	washer21 = tool.washer();
	// set custom mode
	washer21.Custom();
	// init parameters
	washer21.Position(Vec3(-372.39,-60.71,-2.84));
	washer21.Position2(Vec3(-372.39,-58.80,-2.24));
	washer21.ScaleRadius(1.00);
	washer21.Name("conus");
	washer21.InnerDiameter(182.73);
	washer21.OuterDiameter(365.46);
	washer21.Thickness(31.84);
	washer21.Height(47.75);
	washer21.InnerDiameter2(301.79);
	washer21.WasherType(SWasherType::CONUS);
	washer21.details(0.61);
	
	// change the parameters
	washer21.Position(Vec3(-353.83,-60.71,-2.84));
	washer21.Position2(Vec3(-353.83,-58.72,-2.22));
	washer21.ScaleRadius(1.04);
	washer21.InnerDiameter(190.18);
	washer21.OuterDiameter(380.37);
	washer21.Thickness(33.13);
	washer21.Height(49.70);
	washer21.InnerDiameter2(314.10);
	// detail level
	washer21.details(0.41);
	// add screw washer in the scene
	tool.Apply(0);
}
