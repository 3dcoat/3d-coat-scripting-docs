﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//prepare the scene
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create the ngon obj
	NGonPrim ngon8;
	ngon8 = tool.ngon();
	
	// sets position
	ngon8.startPoint(Vec3(238.79,-48.69,340.52));
	ngon8.endPoint(Vec3(238.79,118.53,340.52));
	
	// height & radius
	ngon8.height(167.22);
	ngon8.radius(68.07);
	
	// account of sides
	ngon8.Order(6);
	
	// add in the scene
	tool.Apply(0);
}
