// files counter
int n;
// files list
string files;


void main() {

    n = 0;
    files = "";

    // ask artist whether he would like to run our processing
    bool ok = ModalDialogOkCancel( "Start processing files?", "" );
    if ( !ok ) {
        // stop script if `Cancel` is chosen
        return;
    }

    // for each file call `fileProcessing()` function
    // using `mask` in selected folder
    // filter for file selection
    // for instance, if you need to process only PNG and JPG,
    // put a mask `*.png;*.jpg`
    string mask = "*.png;*.jpg";
    ForEachFileInFolder( "", mask, "fileProcessing" );
    // show result in the end
    ModalDialog( "Files processed:\n" + files, "" );
}




void fileProcessing( string &in fileName ) {

    ++n;
    files += n + ". " + fileName + "\n";
}
