﻿void main() {
    Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	Vox vox1;
	//clear the scene
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create bolt object
	BoltPrim bolt18;
	bolt18 = tool.bolt();
	// sets to custom mode
	bolt18.Custom();
	// position 
	bolt18.Position(Vec3(-336.02,-80.03,161.63));
	// hexa bolt head 
	bolt18.HeadType(SHeadType::HEXA);
	// triangle profile
	bolt18.Profile(ProfileType::TRIANGLE);
	
	// go to surface mode
	vox1.toSurface();
	
	// change axis parameters
	bolt18.AxisX(Vec3(-0.01,-0.22,-0.97));
	bolt18.AxisY(Vec3(0.93,0.36,-0.09));
	bolt18.AxisZ(Vec3(0.37,-0.91,0.20));
	
	bolt18.Diameter(281.71);
	bolt18.Height(93.90);
	bolt18.BodyLength(281.71);
	bolt18.BodyDiameter(140.85);
	bolt18.ThreadLength(187.81);
	bolt18.Pitch(23.48);
	// detail level
	bolt18.details(0.32);
	// add in the scene
	tool.Apply(0);
}
