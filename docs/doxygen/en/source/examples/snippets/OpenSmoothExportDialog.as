// files counter
int n;
// files list
string files;

// access to user's UI
UI ui;


void main(){

    n = 0;
    files = "";

    // for each file call `fileProcessing()` function
    // using `mask` in selected folder
    // filter for file selection
    string mask = "*.stl;*.obj;*.fbx";
    ForEachFileInFolder( "", mask, "fileProcessing" );
    // show result in the end
    ModalDialog( "Files processed:\n" + files, "" );
}




void fileProcessing( string &in fileName ) {

    ++n;

    // 
    SetModalDialogCallback( "callbackDontSave" );
    
    // create new scene
    ui( "$CLEARSCENE" );

    // save file name for future dialogs
    SetFileForFileDialogs( fileName );

    // import object for voxelizing
    ui( "$ImportForVoxelizing" );

    // This is intended to press OK when user will be
    // asked if scale should be kept
    SetModalDialogCallback( "callbackOK" );

    // set merging option - with or without voxelizing
    ui( "$VoxelSculptTool::MergeWithoutVoxelizing", false )
      ( "*Apply" );

    // smooth object
    ui( "$[Page4]Smooth all" );
    
    // change file extension
    string fn = RemoveExtension( fileName );
    fn += "-smoothed.stl";
    
    // save file name for future dialogs
    SetFileForFileDialogs( fn );
    
    // set output filename
    SetModalDialogCallback( "callbackDecimation" );

    // export scene
    ui( "$ExportScene" );
}




void callbackDontSave() {
    // press `Don't save`
    ui( "$DialogButton#2" );
}


void callbackOK() {
    // press `OK`
    ui( "$DialogButton#1" );
}


void callbackDecimation() {
    // set decimation level and press `OK`
    SetSliderValue( "$DecimationParams::ReductionPercent", 80 );
    ui( "$DialogButton#1" );
}
