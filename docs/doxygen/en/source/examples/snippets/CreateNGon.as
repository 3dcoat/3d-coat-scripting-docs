void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a nGon
    Builder builder;
    Mesh mesh = builder.ngon()
      .startPoint( Vec3( 0, 0, 0 ) )
      .endPoint( Vec3( 90, 90, 90 ) )
      .topRadius( 30 )
      .bottomRadius( 40 )
      .relativeHoleRadius( 0.3 )
      // number of sides (polygons)
      .teeth( 3 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put nGon into scene
    room += mesh;
}
