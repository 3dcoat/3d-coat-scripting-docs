void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build an ellipsoid
    Builder builder;
    Mesh mesh = builder.ellipsoid()
      .radius( Vec3( 80, 60, 40 ) )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put ellipsoid into scene
    room += mesh;
}



end of script