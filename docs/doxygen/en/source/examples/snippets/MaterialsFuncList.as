// add new material
DEPRECATED int AddMaterial( string );

// get materials count
DEPRECATED int GetMaterialsCount()

// get material's name
DEPRECATED string GetMaterialName( int )

// get material's index
DEPRECATED int GetMaterialIndex( string );

// rename material
DEPRECATED void RenameMaterial( int index, string );

// delete material
DEPRECATED void DeleteMaterial( int index )

// lock/unlock material
DEPRECATED void LockMaterial( int index, bool )

// make material visible (`true`) or invisible (`false`)
DEPRECATED void SetMaterialVisibility( int index, bool )

// get objects count
DEPRECATED int GetObjectsCount()

// get object's name by its index
DEPRECATED string GetObjectName( int index )

// rename objects
DEPRECATED void RenameObject( int index, string );
// delete objects
DEPRECATED void DeleteObject( int index )

// lock/unlock objects
DEPRECATED void LockObject( int index, bool )

// make object visible (`true`) or invisible (`false`)
DEPRECATED void SetObjectVisibility( int index, bool )

// get faces count
DEPRECATED int GetFacesCount();

// get UV-sets count
DEPRECATED int GetUVSetsCount();

// get UV-set's name by its index
DEPRECATED string GetUVSetName( int index );

// rename UV-set
DEPRECATED void RenameUVSet( int index, string );

// select all faces in current UV-set
DEPRECATED void SelectAllFacesInCurrentUVSet();

// select all faces in current UV-set and Retopo group
DEPRECATED void SelectAllFacesInCurrentUVSetAndGroup();

// select all visible faces in current UV-set
DEPRECATED void SelectAllVisibleFaces();
