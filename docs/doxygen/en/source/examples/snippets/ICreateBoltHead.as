﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	Vox vox1;
	//clear the scene
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	// create bolt head object
	BoltHeadPrim bolthead17;
	bolthead17 = tool.bolthead();
	// init position
	bolthead17.Position(Vec3(-366.07,0.00,196.40));
	// hexa type
	bolthead17.HeadType(SHeadType::HEXA);
	// disable slit
	bolthead17.UseSlit(false);
	bolthead17.Diameter(165.80);
	bolthead17.Height(55.27);
	//enable slit
	bolthead17.UseSlit(true);
	// slit dimensions
	bolthead17.SlitWidth(8.07);
	bolthead17.SlitLength(120.39);
	bolthead17.SlitHeight(5.00);
	// slot 
	bolthead17.SlitType(SSlitType::SLOT);
	//add in the scene
	tool.Apply(0);
}
