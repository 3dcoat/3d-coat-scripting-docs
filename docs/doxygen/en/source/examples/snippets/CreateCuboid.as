void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build cuboid
    Builder builder;
    Mesh mesh = builder.cuboid()
      .side( Vec3( 100, 80, 60 ) )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put cuboid into scene
    room += mesh;
}
