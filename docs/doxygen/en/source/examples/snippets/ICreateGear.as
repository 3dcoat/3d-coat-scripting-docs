﻿void main() {
	Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	//prepare the scene
    SculptRoom sculpt;
	sculpt.toVoxel();
	
	//clear the sculpt room
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume1");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	
	// go to primitives
	ui("$SCULP_PRIM");
	
	// create the gear obj
	GearPrim gear9;
	gear9 = tool.gear();
	
	// position
	gear9.startPoint(Vec3(0.00,0.00,0.00));
	gear9.endPoint(Vec3(0.00,20.00,0.00));
	
	// init parameters
	gear9.height(20.00);
	gear9.radius(10.00);
	gear9.Depth(0.10);
	gear9.Sharpness(0.5);
	gear9.Order(16);
	
	// change parameters
	gear9.startPoint(Vec3(285.31,-181.30,495.22));
	gear9.endPoint(Vec3(285.31,-33.71,478.45));
	gear9.height(148.55);
	gear9.radius(94.87);
	
	// add in the scene
	tool.Apply(0);
}
