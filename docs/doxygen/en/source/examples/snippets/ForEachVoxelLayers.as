// access to a Vox tree
Vox v;


void main() {

    // build a tree
    // look `CreateVoxelTree.as`
    // ...

    // walk through all layers and for each layer call invert() function
    // look below `void invert()`
    v.forRootEach( "invert" );

    // move to "Body" layer and call `invert()` for it and its children
    v.to( "Body" ).call( "invert" ).forEach( "invert" );
}




// function for `forEach()`, look above
void invert() {

    v.isSurface() ? v.toVoxel() : v.toSurface();

    // pause, to see movement in the UI
    Wait( 50 );
}
