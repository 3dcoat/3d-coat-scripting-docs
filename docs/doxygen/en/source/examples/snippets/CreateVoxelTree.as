void main() {

    // prepare scene
    Vox  v;
    v.clearScene();

    // build vox-layers tree
    v
      .appendToParent( "Head" )
      .appendToParent( "Body" )
        .append(         "Neck" )
        .appendToParent( "Left arm" )
        .appendToParent( "Right arm" )
        .appendToParent( "Heart" )
        .appendToParent( "Left leg" )
        .appendToParent( "Right leg" )

      // back to "Head" layer, switch it
      // to "Surface mode" and attach new one
      .to( "Head" ).toSurface()
        .append( "Eye" )
        .appendToParent( "Lamps" )
      // Noticed how last one were added?
      // Correct: `S`, not `V`.
    ;

    // delete first (empty) layer from scene
    v.firstForRoot().remove();
}
