void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a tube
    Builder builder;
    Mesh mesh = builder.tube()
      .startPoint( Vec3( 0, 0, 0 ) )
      .endPoint( Vec3( 30, 50, 70 ) )
      // outer radius of the top of the tube
      .topRadius( 30 )
      // outer radius of the bottom of the tube
      // yep, it can look like a cone
      .bottomRadius( 40 )
      // hole inside tube
      // here - 80% of average outer tube radius
      // tube thickness will be constant:
      //   average radius = (top radius + bottom radius) / 2
      .relativeHoleRadius( 0.8 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put a tube into scene
    room += mesh;
}
