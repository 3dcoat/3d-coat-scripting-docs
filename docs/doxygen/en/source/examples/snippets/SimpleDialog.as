// simple time counter
int t;


void main() {

    t = 0;

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // this function will be called when window appears
    SetModalDialogCallback( "dialogCallback" );
    // show window with simple text message
    ModalDialog( "Some message", "Title" );
}




void dialogCallback() {

    // press the button in several seconds after window appears
    if (t++ > 400) {
      UI ui;
      ui( "$DialogButton#1" );
    }
}
