// access to voxTree
Vox v;


void main() {

    // build a voxel layers tree
    // look `CreateVoxelTree.as`
    // ...

    // access by index for all children of "Head" layer
    v.to( "Body" ).call( "invert" );
    for ( int i = 0; i < v.count(); ++i ) {
        v.at( i ).call( "invert" ).parent();
        Wait( 100 );
    }
}




// look above for function `forEach()`
void invert() {
    v.isSurface() ? v.toVoxel() : v.toSurface();
}
