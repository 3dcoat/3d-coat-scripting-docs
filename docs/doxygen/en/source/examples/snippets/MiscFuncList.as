// Returns whole command line.
string getCommandLine();

// Stores some string as global value that may be read later in
// the session. The value will be stored in 3B file and you will
// be able to read in further work with this scene. 
DEPRECATED void SetGlobalVar(string& Name,string& Value)

// Returns value previously stored using SetGlobalVar.
DEPRECATED string GetGlobalVar (string& Name)

// Returns scene filename (last saved or opened as 3B file).
DEPRECATED string GetSceneFileName()

// Sets scene filename for further saving.
DEPRECATED void SetSceneFileName(string& Name) 

// Highlight element with red rectangle.
// Pass the `time` in seconds.
void HighlightUIElement(string &ID, float time);

// Goes to one of previous dialogs in call stack.
DEPRECATED void back(int steps=1);

// Opens window described by xml-file pointed by Path.
// If Path contains .3b file will be opened as 3B file.
DEPRECATED void open(string &Path);

// Opens model for PPP, if path is empty, shows open dialog.
DEPRECATED void ppp(string &path);

// Opens model for MV painting, if path is empty, shows open dialog.
DEPRECATED void mv(string &path);

// Opens model for Ptex, if path is empty, shows open dialog.
DEPRECATED void ptex(string &path);

// Import image as mesh, dialog will be shown.
DEPRECATED void imagemesh();

// Import mesh as reference, if path is empty dialog will be shown.
DEPRECATED void refmesh(string &path);

// Import mesh for vertex painting, if path is empty dialog will be shown.
DEPRECATED void vertexpaint(string &path);

// Perform autopo over the mesh chosen in dialog.
DEPRECATED void autopo(string &path);

// Opens mesh for repairing. If id contains "vox" then model will be
// voxelized, if there is substring "shell" then mesh will be imported
// as thin shell. Mesh Opening dialog will be shown.
DEPRECATED void repair(string &id);

// Activate bas-relief tool.
DEPRECATED void bass();

// Activale remove undercuts mode.
DEPRECATED void undercut();

// Activate special voxel tool. id may be found in English.xml between
// <ID>...</ID> if you will find name of tool between
// <Text>...</Text> tags.
DEPRECATED void activate(string &id);

// Activate retopo tool.
DEPRECATED void retopo();

// Open mesh using dialog and merge as retopo mesh.
DEPRECATED void retopopen();

// Activate any room - name is one of "Paint", "Tweak", "UV",
// "Voxels", "Retopo", "Render".
DEPRECATED void ToRoom(string &name);

// Check if you are in specified room - name is one of "Paint",
// "Tweak", "UV", "Voxels", "Retopo", "Render".
DEPRECATED bool IsInRoom(string &name);

// Add new volume in voxel room. If name is empty name will be
// assigned automatically.
DEPRECATED void AddNewVolume(string &name);

// Activate UV room.
DEPRECATED void uv();

// Activate voxel room and add new volume.
DEPRECATED void vox();

// Create sphere of radius R in voxel room in current object.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
DEPRECATED void sphere(float x,float y,float z,float r,int mode);

// Create cube in voxel room in current object.
// \param mode 0 - add, 1 - subtract, 2 - intersect with scene.
// \param sizex Size by X-axis.
// \param sizey Size by Y-axis.
// \param sizez Size by Z-axis.
DEPRECATED void cube(float x,float y,float z,float sizex,float sizey,float sizez,int mode);

// Turn all volumes to surface mode.
DEPRECATED void surf();

// Turn current volume to the surface mode.
DEPRECATED void cursurf();

// Turn current volume to voxel mode, voxelize if need.
DEPRECATED void voxelize();

// Sets merging options in voxel room. opt is just set of substrings
// with different options. Possible values are:
// [voxelize=true]
// [voxelize=false]
// [separate=true]
// [separate=false]
// [respectneg=true]
// [respectneg=false]
// [as_skin=true]
// [as_skin=false]
// [skin=....] - to set skin thickness.
DEPRECATED void mergeopt(string &opt);
// Example:
mergeopt( "[voxelize=true][as_skin=true][skin=4.5]" );

// Merge model in voxel room. Empty string means that dialog will be shown.
DEPRECATED void merge(string &model);

// Activate voxel primitives tool. Possible primitives:
// cube, cylinder, sphere, tube, cone, ellipse, n-gon, gear.
DEPRECATED void prim(string &id);

// Apply in current tool (same as press enter).
DEPRECATED void apply();

// Apply in Merge tool without asking "Keep scale?".
// Scale will not be kept and scene scale will not be changed.
DEPRECATED void ApplyAndKeepScale();

// Apply in current tool (same as press enter) wint one difference -
// in Merge tool scale of merged object sill be automatically kept and
// scene scale changed if this merge is first.
DEPRECATED void mapply();

// Open recent 3B-file.
DEPRECATED void recent3b();

// Print text to MyDocuments/3D-CoatV4/log.txt.
DEPRECATED void Log(string &line);

// Generate integer random number min..max.
int rand(int min,int max);

// Generate floating random number min..max.
float randF(float min,float max);

// Set random generator seed.
void seed(int val);

// Show progress bar pos = 0..100.
DEPRECATED void ProgressBar(const string& message,int pos);

// Set orthogonal (true) or perspective (false) view mode.
DEPRECATED void SetOrthoMode(bool value);
