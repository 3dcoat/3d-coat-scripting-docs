﻿void main() {

   	Coat coat;
	// go to sculpt room
	coat.room("Sculpt");
	
	// voxel object
	Vox vox1;
	//clear the sculpt room
	vox1.clearScene();
	vox1.rename("Volume2");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");

	//prepare the primitives tool
	PrimitivesTool tool;
	
	// create the lathe object
	LathePrim lathe10;
	lathe10 = tool.lathe();
	
	// sets axises
	lathe10.AxisX(Vec3(0.22,0.08,0.97));
	lathe10.AxisY(Vec3(-0.09,0.99,-0.06));
	lathe10.AxisZ(Vec3(-0.97,-0.07,0.23));
	
	// sets position
	lathe10.Position(Vec3(-141.10,35.58,306.22));
	
	// sets sides
	lathe10.SideA(160.96);
	lathe10.SideB(160.96);
	lathe10.SideC(160.96);
	
	// Cylinder
	lathe10.LatheType(0);
	
	//points coordinates
	lathe10.SetPoint(0,0.00,0.50,0);
	lathe10.SetPoint(1,0.49,0.11,0);
	lathe10.SetPoint(2,1.00,0.50,0);

	
	// add lathe in the scene
	tool.Apply(0);
}
