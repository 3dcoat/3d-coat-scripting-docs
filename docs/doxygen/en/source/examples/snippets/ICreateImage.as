﻿void main() {

    Coat coat;
	// go to sculpt room
	coat.room("Sculpt");
	
	// voxel object
	Vox vox1;
	//clear the sculpt room
	vox1.clearScene();
	vox1.rename("Volume3");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	
	//create the image object
	ImagePrim image12;
	image12 = tool.image();
	
	image12.AxisX(Vec3(0.80,0.00,-0.60));
	image12.AxisY(Vec3(0.00,1.00,0.00));
	image12.AxisZ(Vec3(0.60,0.00,0.80));
	
	// Pos & sides
	image12.Position(Vec3(-257.19,0.00,282.29));
	image12.SideA(128.02);
	image12.SideB(128.02);
	image12.SideC(12.80);
	
	// rotation
	image12.ExtraRotation(0.00);
	// bending
	image12.Bend(false);
	
	ImageMeshParam imparam1;
	
	// sets the size in the scene
	imparam1.SizeInScene(100.00);
	
	// sets the pictures 
	imparam1.Top("/images/Bracelet.png");
	imparam1.TopBump("/images/Bracelet.png");
	
	// picture weight
	imparam1.TopBottomWeight(0.70);
	
	imparam1.BasicThickness(8.00);
	imparam1.BumpThickness(8.00);
	
	// pick the image
	image12.PickImage();
	
	// resize object
	image12.SideA(91.12);
	image12.SideB(91.12);
	image12.SideC(9.11);
	
	// add the image in the scene
	tool.Apply(0);
}
