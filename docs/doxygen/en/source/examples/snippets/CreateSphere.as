void main() {

    // prepare scene
    SculptRoom room;
    // clear the Sculpt room a switch it to Surface mode
    room.clear().toSurface();

    // build a sphere
    Builder builder;
    Mesh mesh = builder.sphere()
      .radius( 70 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put sphere into scene
    room += mesh;
}
