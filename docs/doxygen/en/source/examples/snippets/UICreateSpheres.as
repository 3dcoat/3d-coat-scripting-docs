// access to debugging
Debug dbg;
DebugLog log = dbg.log();


void main() {

    log.clear();

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a sphere
    UI ui;
    ui.toSculptRoom();
    // call UI elements on screen
    // MClick + RClick to get desired element's ID
    // use `*Apply` instead of `Apply`, that 3DCoat may know
    // that we want to *press* the button
    ui( "Primitives" )( "Create sphere" )
      ( "$SpherePrim::%$px4[62]", 0 )
      ( "$SpherePrim::%$py5[62]", 0 )
      ( "*Apply" );

    // one more beside
    ui( "$SpherePrim::%$px4[62]", 50 )
      ( "*Apply" );

    // and one more above
    ui( "$SpherePrim::%$py5[62]", 50 )
      ( "*Apply" );
}
