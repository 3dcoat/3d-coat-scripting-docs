// access to debugging
Debug dbg;


void main() {

    // text log
    // open from "Script / Show Execution Log"
    DebugLog log = dbg.log();
    // prepare text log
    log.clear();

    // viewport
    // see graphics immediately
    DebugDraw drw = dbg.draw();
    // prepare the viewport
    //   1) clear
    //   2) choose default brush color
    drw.clear().color( 0xFFFFEE00 );

    // write here what would you like to show / display
    // ...
}
