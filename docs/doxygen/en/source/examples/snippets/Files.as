// access to debugging
Debug dbg;


void main() {

    // prepare text log
    DebugLog log = dbg.log();
    log.clear();

    // access to a file "a.txt" through variable `a`
    // pay attention: file won't be created here
    File  a( "a.txt" );
    if ( a.exists() ) {
        // remove existing file
        a.remove();
    }

    a += "Line for demo...\n"
         "and yet one, yes!";


    File  b( "b.data" );
    b.binary( true );
    string  ext = b.extension();
    log += "extension for `b` is " + ext;

    File  c( "my/folder/c.txt" );
    string fullPath = c.fullPathFile();
    log += "full path for `c` is " + fullPath;

    string path = c.path();
    log += "path for `c` is " + path;
    
    FormatBinary  bin = { 0x46, 0x4c, 0x49, 0x46 };
    b += bin;
    
    //b.remove();
}
