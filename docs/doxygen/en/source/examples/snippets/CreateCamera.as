Debug     dbg;
DebugLog  log = dbg.log();
void main() {

	// prepare scene
    SculptRoom  sculpt;
	Builder  builder;
	const Vec3  pa( 0 );
	
	sculpt.clear().toSurface();
	
	// create cube primitive
	Mesh  a = builder.cuboid()
		.side( Vec3( 500, 400, 300 ) )
		.position( pa )
		.details( 0.1 )
		.build();

	sculpt += a;
	
	CameraShortcut camera;
	// set camera state 
	CameraShortcut::ViewState state = camera.State();
    state.Fov(105.0);
	state.Position(Vec3(0));
	camera.State(state);

	CameraView cameraView;
	// set camera view
	log += "CameraView::OrthoState";
	log += CameraView::OrthoState::Back;
	cameraView.SetView(CameraView::OrthoState::Back);

}
