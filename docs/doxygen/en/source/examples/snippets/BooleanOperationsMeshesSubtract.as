void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // create two spheres of different sizes
    // that overlap
    Builder builder;
    Mesh a = builder.sphere()
      .radius( 70 )
      .details( 0.1 )
      .build();
    Mesh b = builder.sphere()
      .radius( 40 )
      .position( Vec3( 30, 40, 50 ) )
      .details( 0.5 )
      .build();

    // subtract meshes
    Mesh result = a - b;

    // put resulting mesh into scene
    room += result;
}
