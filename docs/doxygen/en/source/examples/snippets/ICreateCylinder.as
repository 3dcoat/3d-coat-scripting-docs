﻿void main() {

    Coat coat;
	UI ui;
	coat.room("Sculpt");
	
	//prepare the scene
    SculptRoom sculpt;
	sculpt.clear();
	sculpt.toVoxel();
	
	//prepare the vox
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume3");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	ui("$SCULP_PRIM");
	
	// gets cylinder
	CylinderPrim cylinder4;
	cylinder4 = tool.cylinder();
	
	cylinder4.useFillet(true);
	cylinder4.filletRadiusRelation(0.20);
	
	cylinder4.startPoint(Vec3(-280.98,23.17,236.68));
	cylinder4.endPoint(Vec3(-292.61,159.80,242.19));
	
	cylinder4.height(137.24);
	cylinder4.radius(52.42);
	cylinder4.topRadius(52.42);
	cylinder4.bottomRadius(52.42);
	cylinder4.scalex(1.00);
	cylinder4.scaley(1.00);
	cylinder4.details(0.69);
	
	// add in the scene
	tool.Apply(0);
	
	cylinder4.startPoint(Vec3(-251.18,102.24,277.48));
	cylinder4.endPoint(Vec3(-221.85,103.49,308.54));
	cylinder4.height(42.73);
	cylinder4.radius(27.30);
	cylinder4.topRadius(27.30);
	cylinder4.bottomRadius(27.30);
	
	// add in the scene
	tool.Apply(0);
	
	cylinder4.startPoint(Vec3(-335.49,85.39,257.88));
	cylinder4.endPoint(Vec3(-394.83,79.38,281.57));
	cylinder4.height(64.17);
	
	// add in the scene
	tool.Apply(0);
	
	cylinder4.startPoint(Vec3(-295.43,127.83,244.86));
	cylinder4.endPoint(Vec3(-306.96,263.31,250.33));
	cylinder4.height(136.08);
	
	// add in the scene
	tool.Apply(0);

}
