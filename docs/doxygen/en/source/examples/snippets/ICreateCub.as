﻿void main() { 

	Coat coat;
	// go to sculpt room
	coat.room("Sculpt");
	
	// voxel object
	Vox vox1;
	//clear the sculpt room
	vox1.clearScene();
	vox1.rename("Volume3");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	
	// gets Cube prim 
	CubPrim cube2;
	cube2 = tool.cube();
	
	//initializing cube
	cube2.AxisX(Vec3(1.00,0.00,0.00));
	cube2.AxisY(Vec3(0.00,1.00,0.00));
	cube2.AxisZ(Vec3(0.00,0.00,1.00));
	
	// no fillet mode
	cube2.useFillet(false);
	
	cube2.Position(Vec3(0.00,0.00,0.00));
	cube2.SideA(30.00);
	cube2.SideB(30.00);
	cube2.SideC(30.00);
	
	// go tо the surface mode
	vox1.toSurface();
	
	// sets fillet mode
	cube2.useFillet(true);
	
	// sets radius
	cube2.filletRadiusRelation(0.20);
	cube2.filletRadius(14.41);
	
	// sets position
	cube2.Position(Vec3(-223.11,44.94,336.39));
	
	// sets sides
	cube2.SideA(144.11);
	cube2.SideB(144.11);
	cube2.SideC(144.11);
	cube2.details(0.38);
	
	// add the cube in the scene 
	tool.Apply(0);
}
