﻿void main() {
	Coat coat;
	// go to the retopo room
	coat.room("Retopo");
	
	// clear room
	RetopoRoom retopo;
	retopo.clear();
	
	// prepare the primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to primitives
	cmd("$RTP_PRIM");
	
	// create the tube
	rGeoTube rtube29;
	rtube29 = tool.rtube();
	
	// init flags
	rtube29.useDiameter(false);
	rtube29.useFillet(false);
	rtube29.useUniform(true);
	
	// init start & end points
	rtube29.startPoint(Vec3(0.00,0.00,188.41));
	rtube29.endPoint(Vec3(0.00,119.83,188.41));
	
	// set a height
	rtube29.height(119.83);
	
	// set a radius
	rtube29.radius(85.30);
	
	// set a top radius
	rtube29.topRadius(85.30);
	
	// set a bottom radius
	rtube29.bottomRadius(85.30);
	
	// set a wall thickness
	rtube29.WallThickness(42.65);
	
	// set scales to "1"
	rtube29.scalex(1.00);
	rtube29.scaley(1.00);
	
	// set the number of divisions 
	rtube29.DivY(7);
	rtube29.DivX(1);
	rtube29.DivZ(16);
	rtube29.AverageDivision(8);
	
	// add in the scene
	tool.Apply();
}
