﻿void main() {
	Coat coat;
	
	// go to Retopo room
	coat.room("Retopo");
	
	// clear retopo
	RetopoRoom retopo;
	retopo.clear();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	coat.room("Retopo");
	
	// go to primitives
	cmd("$RTP_PRIM");
	
	// create the ellipse
	rGeoEllipse rellipse26;
	rellipse26 = tool.rellipse();
	
	// init axes & position 
	rellipse26.AxisX(Vec3(1.00,-0.04,0.04));
	rellipse26.AxisY(Vec3(0.04,1.00,-0.04));
	rellipse26.AxisZ(Vec3(-0.04,0.05,1.00));
	rellipse26.Position(Vec3(-279.27,11.38,-12.08));
	
	// init sided
	rellipse26.SideA(30.00);
	rellipse26.SideB(30.00);
	rellipse26.SideC(62.75);
	
	// change axes
	rellipse26.AxisX(Vec3(0.34,0.03,0.94));
	rellipse26.AxisZ(Vec3(-0.94,0.06,0.34));
	
	// change position 
	rellipse26.Position(Vec3(-283.85,16.57,98.84));
	
	// add in the scene
	tool.Apply();
}
