﻿void main() {

	Coat coat;
	UI ui;
	// go to sculpt room
	coat.room("Sculpt");

	// clear the sculpt room 
	SculptRoom sculpt;
	sculpt.clear();
	sculpt.toVoxel();
	
	// get vox object
	Vox vox1;
	vox1.clearScene();
	vox1.rename("Volume12");
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// gp to primitives
	ui("$SCULP_PRIM");
	
	//gets ellipse tool 
	EllipsePrim ellipse3;
	ellipse3 = tool.ellipse();

	// init parameters
	ellipse3.Position(Vec3(-307.18,61.64,380.09));
	ellipse3.SideA(40.82);
	ellipse3.SideB(40.89);
	ellipse3.SideC(108.31);
	ellipse3.AxisX(Vec3(1.00,-0.04,-0.01));
	ellipse3.AxisY(Vec3(0.04,0.99,0.15));
	ellipse3.AxisZ(Vec3(0.00,-0.15,0.99));
	
	// go to surface mode
	vox1.toSurface();
	
	// set new sides
	ellipse3.SideA(51.39);
	ellipse3.SideB(51.48);
	ellipse3.SideC(136.36);
	// sets detail level
	ellipse3.details(0.61);
	//add in the scene
	tool.Apply(0);
}
