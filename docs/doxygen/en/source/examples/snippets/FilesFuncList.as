// Executes command, pass parameters. You may run executables,
// open webpages, open files in corresponding external editors.
DEPRECATED void Execute(const string& command, const string& parameters);
// 3DCoat script example - How to pass 3DCoat installation folder
// into python script under Linux:
    void main() {
        string instPath = installPath();
        string userFolder = homePath(); // userFolder = "/home/YourUserName"
        string pythonInterpreter = "/usr/bin/env python"; // Similar to standard "#!/usr/bin/env python"
        string pythonScript = userFolder + "/bin/testLauncher"; // Path to python script
        string arg = "-f " + "\"" + instPath + "\"";
        string app = pythonInterpreter + " " + pythonScript;
        print(app + "\n" + arg + "\n");
        Execute(app, arg);
    }

// Reads file content into the string.
DEPRECATED string ReadFromFile(const string& filename);

// Writes string content to the file.
// \param append `true` - string will be appended to file,
//               `false` - string will be overwrote a file.
DEPRECATED void WriteToFile(const string& filename,const string& text,bool append);

// Checks if file exists.
DEPRECATED bool CheckIfFileExists(const string& filename);

// Removes file from the disk.
DEPRECATED void RemoveFile(const char* filename);

// Removes a path from the `s`.
DEPRECATED string RemoveFilePath(string &in s);

// Removes a file extension from the `s`.
DEPRECATED string RemoveExtension(string &in s);

// Converts `s` to absolute path.
DEPRECATED string EnsureAbsolutePth(string &in s);

// Returns a path from the `s`.
DEPRECATED string GetFilePath(string &in s);

// Returns a file extension from the `s`.
DEPRECATED string GetFileExtension(string &in s);

// Returns a file name from the `s`.
DEPRECATED string GetFileName(string &in s);

// `true` then a file exists by the path `s`.
DEPRECATED bool CheckIfExists(string &in s);

// Creates all folders to the path `s`.
DEPRECATED void CreatePath(string &in s);

// Calls Callback for each file in folder. If Folder is empty
// used will be asked to choose file in folder.
// Callback should be declared as `void Callback(string &in Path)`.
// `ExtList` contains list of extensions like "*.jpg;*.png"
DEPRECATED void ForEachFileInFolder(string &in Folder,string &in ExtList,string &in Callback);

// Returns a path where the 3DCoat was installed.
string installPath();

// Get an absolute path including write folder of the 3DCoat.
string rwPath( const string& userPath );

// Returns a path to home directory "~" (for example "/home/YourUserName").
string homePath();
