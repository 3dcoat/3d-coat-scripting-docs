﻿void main() {
    Coat coat;
	UI ui;
	//go to sculpt room
	coat.room("Sculpt");
	
	Vox vox1;
	//clear the scene
	vox1.clearScene();
	vox1.rename("Volume36");
	vox1.toSurface();
	
	//prepare the primitives tool
	PrimitivesTool tool;
	// go to primitives
	ui("$SCULP_PRIM");
	// create screw object
	TapScrewPrim screw19;
	screw19 = tool.tapscrew();
	// sets custom mode
	screw19.Custom();
	// go to surface mode
	vox1.toSurface();
	// axises
	screw19.AxisX(Vec3(0.03,0.03,-1.00));
	screw19.AxisY(Vec3(0.98,0.18,0.03));
	screw19.AxisZ(Vec3(0.18,-0.98,-0.03));
	// init position
	screw19.Position(Vec3(-239.87,-45.06,255.75));
	//other parameters
	screw19.HeadDiameter(210.99);
	screw19.HeadHeight(70.33);
	screw19.HeadType(SHeadType::BUGLE);
	//enable slit
	screw19.UseSlit(true);
	screw19.SlitWidth(10.55);
	screw19.SlitLength(216.26);
	screw19.SlitHeight(17.58);
	screw19.SlitType(SSlitType::SLOT);
	screw19.Length(281.31);
	screw19.Diameter(70.33);
	screw19.UseThread(true);
	screw19.UseGroove(false);
	screw19.ThreadLength(210.99);
	screw19.ThreadDiameter(84.39);
	screw19.Pitch(17.58);
	screw19.details(0.55);
	// add screw in the scene
	tool.Apply(0);
}
