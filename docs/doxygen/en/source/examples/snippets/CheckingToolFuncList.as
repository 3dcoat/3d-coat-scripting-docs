// Get current mouse coordinate X.
DEPRECATED float GetMouseX()

// Get current mouse coordinate Y.
DEPRECATED float GetMouseY()

// Get pen pressure. 
DEPRECATED float GetPressure()

// Check if LMB pressed. 
DEPRECATED bool LMBPressed()

// Check if RMB pressed. 
DEPRECATED bool RMBPressed()

// Check if MMB pressed. 
DEPRECATED bool MMBPressed();

// Get visible (in screen projection) radius of the brush. 
DEPRECATED float GetVisiblePenRadius();

// Start drawing stroke at point (x,y) with some pressure.
// \param Pressure In diapason [ 0.0 (without pressure); 1.0 (maximum pressure) ].
DEPRECATED void StartStroke(float x,float y,float Pressure)

// Draw stroke to point (x,y) with some pressure.
// \param Pressure In diapason [ 0.0 (without pressure); 1.0 (maximum pressure) ].
DEPRECATED void DrawStrokeTo(float x,float y,float Pressure)

// After that command stroke will be actually drawn.
// Set of commands StartStroke. DrawStrokeTo should be
// terminated by EndStroke.
DEPRECATED void EndStroke()

// True when object presents by ray (x,y).
DEPRECATED bool ScreenRayPicksObject(float x,float y)


// Was widget with identifier ID recently (within last Time sec) pressed?
DEPRECATED bool WasRecentlyPressed(string &in ID,float Time);

// Was widget with identifier ID recently (within last Time sec)
// pressed via RMB?
DEPRECATED bool WasRecentlyRMBPressed(string &in ID,float Time);

// Is user in tool identified as ID? To get current tool
// identifier press RMB + MMB over empty field.
DEPRECATED bool IsInTool(string &in ToolID);

// Get active tool ID.
DEPRECATED string GetCurrentToolID ();

// Get time (sec) since script started.
DEPRECATED float GetTimeSinceStart();
