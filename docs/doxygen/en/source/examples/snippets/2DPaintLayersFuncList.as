// add new layer for painting
void AddPaintLayer( string );

// get current layer's name
string GetCurrentPaintLayerName();

// rename current layer
void RenameCurrentPaintLayer( string );

// select layer by its name
bool SelectPaintLayer( string )

// select bottom layer
void SelectFirstPaintLayer( bool )

// select next layer
bool SelectNextPaintLayer( bool )

// get layer visibility (true/false)
bool GetPaintLayerVisibility()

// make layer visible (`true`) or invisible (`false`)
void SetPaintLayerVisibility( bool )

// set layer's opacity (`0 .. 100`)
void SetPaintLayerOpacity( float )

// set layer's depth channel opacity (`0 .. 100`)
void SetPaintLayerDepthOpacity( float )
