void main() {

    // prepare scene
    SculptRoom room;
    room.clear().toSurface();

    // build a gear
    Builder builder;
    Mesh mesh = builder.gear()
      .startPoint( Vec3( 0, 0, 0 ) )
      .endPoint( Vec3( 90, 90, 90 ) )
      .topRadius( 30 )
      .bottomRadius( 50 )
      .relativeHoleRadius( 0.3 )
      // how close teeth are to the inner radius
      .depth( 0.5 )
      // teeth sharpness, 0 - the sharpest
      .sharpness( 0.2 )
      // teeth number
      .teeth( 3 )
      // details (wireframe density), 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // put a gear into scene
    room += mesh;
}
