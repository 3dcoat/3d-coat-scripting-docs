﻿void main() {
	
	Coat coat;
	// go to sculpt room
	coat.room("Sculpt");
	
	// voxel object
	Vox vox1;
	//clear the sculpt room
	vox1.clearScene();
	vox1.rename("Volume2");

	// go to primitives
	UI ui;
	ui("$SCULP_PRIM");
	
	//create the font object
	Font font;
	
	//font parameters
	font.Name("Arial");
	font.Size(96);
	font.Weight(400);
	font.Style(0);
	font.OutPrecision(3);
	font.ClipPrecision(2);
	font.CharSet(0);
	font.Quality(4);
	font.Family(34);

	//prepare the primitives tool
	PrimitivesTool tool;
	
	//create the text object
	TextPrim text11;
	text11 = tool.text();
	
	//sets text
	text11.SetText("Sculpt room");
	
	//axis transform
	text11.AxisX(Vec3(0.77,0.00,-0.64));
	text11.AxisY(Vec3(-0.34,0.85,-0.41));
	text11.AxisZ(Vec3(0.54,0.53,0.65));
	
	//position
	text11.Position(Vec3(-293.23,132.34,210.24));
	
	//sides
	text11.SideA(174.53);
	text11.SideB(174.53);
	text11.SideC(17.45);
	
	//rotation
	text11.ExtraRotation(-21.60);
	
	//bending
	text11.Bend(false);
	
	//add text object in the scene
	tool.Apply(0);
}