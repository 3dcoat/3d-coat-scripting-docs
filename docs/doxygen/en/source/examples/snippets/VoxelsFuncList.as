// Returns true if current volume is in surface mode.
DEPRECATED bool IsSurface();

// Check if volume cached.
DEPRECATED bool IsInCache();

// Move current volume to cache.
DEPRECATED void ToCache();

// Restore current volume from cache.
DEPRECATED void FromCache();

// Get current volume name.
DEPRECATED string GetCurVolume();

// Rename current volume.
DEPRECATED void RenameCurVolume(string &name);

// Set surface/voxel mode for volume Surf=true - set surface mode,
// false - volume. It is same as click on S/V icon.
// If silent=true then no dialogs will be shown, all will be
// done by default.
DEPRECATED void SetCurVolumeMode(bool Surf,bool Silent);

// Set current volume by name, returns true if succeed.
DEPRECATED bool SetCurVolume(string &name);

// Select first volume in scene, if OnlyVisible==true then first
// visible volume will be selected.
DEPRECATED void SelectFirstVolume(bool OnlyVisible);

// Select next volume after current in tree, if OnlyVisible==true
// then next visible volume will be selected. Returns false if
// current volume is last in list.
DEPRECATED bool SelectNextVolume(bool OnlyVisible);
// Example of walking through all volumes:
    void main() {
        string s=GetCurVolume(); //keep current selection
        SelectFirstVolume(true);
        do {
            //insert your action
        } while( SelectNextVolume(true));
        SetCurVolume(s); //restore initial selection
    }

// Checks if volume is empty.
DEPRECATED bool CurVolumeIsEmpty();

// Get current volume polycount.
DEPRECATED int GetCurVolumePolycount();

// Get polycount of whole voxel scene.
DEPRECATED int GetVoxSceneVisiblePolycount();

// Get polycount of visible volumes.
DEPRECATED int GetVoxScenePolycount ();

// Get current volume shader name.
DEPRECATED string GetCurVolumeShader();

// Set volume visibility (like with eye icon).
DEPRECATED void SetVolumeVisibility(bool vis)

// Returns current volume visibility.
DEPRECATED bool GetVolumeVisibility()

// Sets Ghost property to volume.
DEPRECATED void SetVolumeGhosting(bool Ghosting)

DEPRECATED void SetCurVolumeTransform(Mat4& transform)

// Set/get current volume transform as 4-dimentional matrix.
// See the Mat4 class description.
DEPRECATED Mat4 SetCurVolumeTransform()

DEPRECATED bool GetVolumeSquare()

// Get square and volume of the current volume.
DEPRECATED bool GetVolumeVolume()

// Get Ghost property from the volume.
DEPRECATED bool GetVolumeGhosting()

// Set opacity of the current volume if the shader
// has corresponding property.
DEPRECATED void SetVolumeOpacity(float Opacity)

// Set current volume color if this property of shader is available.
DEPRECATED void SetVolumeColor(int Color)

// Assign val to current object shader property field.
DEPRECATED void SetShaderProperty(string &id, string &val)
