// add new retopo layer
DEPRECATED void AddPaintLayer( string );

// get current retopo layer name
DEPRECATED string GetCurrentRetopoLayerName();

// rename layer
DEPRECATED void RenameCurrentRetopoLayer( string );

// select retopo layer by its name
DEPRECATED bool SelectRetopoLayer( string )

// select first retopo layer
DEPRECATED void SelectFirstRetopoLayer( bool )

// select next retopo layer
DEPRECATED bool SelectNextRetopoLayer( bool )

// get retopo layer visibility
DEPRECATED bool GetRetopoLayerVisibility()

// make visible (`true`) or invisible (`false`) current retopo layer
DEPRECATED void SetRetopoLayerVisibility( bool )
