﻿/**

\page ATransformMeshes  🌀 Mesh Transform


If you already [can create meshes](\ref APrimitives),
then you're ready to learn how to transform them.

Now with scripts you may make 3 changes to a mesh like:

  -# Mesh position
  -# Rotation
  -# Scale




\section ATransformMeshesPosition  Position

Complete example for changing coordinates of the existing mesh:

\include "snippets/TransformCapsulePosition.as"

For mesh transformation this class is used
[ToolsMeshTransform](\ref coat::scripto::ToolsMeshTransform).




\section ATransformMeshesRotation  Rotation

We'll skip mesh creation part of the code for a more compact view in the following examples.

\include "snippets/TransformCapsuleRotation.as"




\section ATransformMeshesScale  Scale

\include "snippets/TransformCapsuleScale.as"




\section ATransformMeshesAll  Together

With fluent-interface you may simply write your code like this:

\include "snippets/TransformCapsuleAll.as"


\see \ref ABooleanOperationsMeshes
\see \ref AMath


**/
