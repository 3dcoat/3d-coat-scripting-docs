﻿/**

\page AUI  🌀 Interacting with user interface (UI)

We tried to simplified access to 3DCoat's UI from scripts.

You can select any menu item, press any button by writing a correspondent code.
\code
UI ui;
ui( "TitleThatYouSeeOnScreen" );
\endcode

You may ask: "What about data fields"?
You can get access to them by using internal name.

Example:

\include "snippets/UICreateSpheres.as"

Because 3DCoat is multilingual, there is nuance.

In order for your script to work properly in 3DCoat instance that uses different UI language use code (starts with `$`) or switch the UI language in the script. It is, of course, good to switch it back when you finish.
\code
string currentLang = ui.lang();
// if your script were written for English interface
ui.lang( "english" );

// your script

// switch back to the default language used by an artist
ui.lang( currentLang );
\endcode


Some other useful functions:

\include "snippets/UIMoreFuncList.as"


You may need these functions:

\include "snippets/CheckingToolFuncList.as"


Also check this full list of functions:

\include "snippets/MiscFuncList.as"


\see \ref ABuildOwnDialogs
\see \ref ABuildDialogs

**/
