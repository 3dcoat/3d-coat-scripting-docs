﻿/**

\page AFiles  🌀 Working with files

You may want to write the results of your work to a file and/or read some data from a file.

3DCoat can do that. Use these commands:

\code
// allows access to the file "a.txt" through variable `a`
// take a note: file will not be created here
File  a( "a.txt" );
\endcode

\code
// returns `true`, if file exists (access via `a` variable)
File  a( "a.txt" );
if ( a.exists() ) {
    // do something
}
\endcode

\code
// set a mode that allows input / output
// nor only with text symbols, but also with bytes;
// which allows you read / create your own binary files
File  a( "a.data" );
a.binary( true );

// input / output is in text format
a.binary( false );

// returnes if a file is opened in binary mode
bool  r = a.binary();
\endcode

\code
// returns file extension
File  a( "a.txt" );
string  ext = a.extension();
\endcode

\code
// returns full path (folder path and file name)
File  a( "my/folder/a.txt" );    
string  fullPath = a.fullPathFile();
\endcode

\code
// returnes file path (only folder)
File  a( "my/folder/a.txt" );
string  path = a.path();
\endcode

\code
// replaces existing file data with our text
File  a( "a.txt" );
a = "Line for demo...\n"
    "and yet one, yes!";
\endcode

\code
// adds our text to the existing data
File  a( "a.txt" );
a += "Line for demo...\n"
     "and yet one, yes!";
\endcode

\code
// replaces existing file data with our binary data
File  a( "a.bin" );
FormatBinary  bin = { 0x46, 0x4c, 0x49, 0x46 };
a = bin;
\endcode

\code
// adds our binary data to the existing file data 
File  a( "a.bin" );
FormatBinary  bin = { 0x46, 0x4c, 0x49, 0x46 };
a += bin;
\endcode

\code
// deletes file
File  a( "a.txt" );
a.remove();
\endcode


You may also need to open a dialog window for file selection.
Here's the list of functions available:

\include "snippets/BuildFileDialogsFuncList.as"


Also if you prefer functional programing you may use these functions for files operations:

\include "snippets/FilesFuncList.as"


**/
