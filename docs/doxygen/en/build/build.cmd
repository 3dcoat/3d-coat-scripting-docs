@echo off

@if "%1"=="" goto NeedParams
@goto Start


:NeedParams
@echo Configure filename must be present like first param.
@goto End


:Start
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set starttime=%%a:%%b)
echo Start time %starttime%

set "currentconf=%1"
echo Current configure file is "%currentconf%"

for /f "delims=" %%i in ("%0") do set "currentpath=%%~dpi"
echo Current path is "%currentpath%"
cd "%currentpath%"

rmdir /S /Q "%currentpath%\..\output"
doxygen "%currentconf%"
explorer "..\output\html\index.html"


:End
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set stoptime=%%a:%%b)
echo Stop time %stoptime%
