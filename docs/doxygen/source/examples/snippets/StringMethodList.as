// Returns the length of the string.
uint length() const;

// Sets the length of the string.
void resize(uint);

// Returns true if the string is empty, i.e. the length is zero.
bool isEmpty() const;

// Returns a string with the content starting at start and the
// number of bytes given by count. The default arguments will
// return the whole string as the new string.
string substr(uint start = 0, int count = -1) const;

// Inserts another string other at position pos in the original string.
void insert(uint pos, const string &in other);

// Erases a range of characters from the string, starting at position
// pos and counting count characters.
void erase(uint pos, int count = -1);

// Find the first occurrence of the value str in the string,
// starting at start. If no occurrence is found a negative value
// will be returned.
int findFirst(const string &in str, uint start = 0) const;

// Find the last occurrence of the value str in the string.
// If start is informed the search will begin at that position, i.e.
// any potential occurrence after that position will not be searched.
// If no occurrence is found a negative value will be returned.
int findLast(const string &in str, int start = -1) const;

// The first variant finds the first character in the string that matches
// on of the characters in chars, starting at start. If no occurrence is
// found a negative value will be returned.
// The second variant finds the first character that doesn't match any of
// those in chars. The third and last variant are the same except they
// start the search from the end of the string.
// These functions work on the individual bytes in the strings. They
// do not attempt to understand encoded characters, e.g. UTF-8 encoded
// characters that can take up to 4 bytes.
int findFirstOf(const string &in chars, int start = 0) const;
int findFirstNotOf(const string &in chars, int start = 0) const;
int findLastOf(const string &in chars, int start = -1) const;
int findLastNotOf(const string &in chars, int start = -1) const;

// Splits the string in smaller strings where the delimiter is found.
array<string> split(const string &in delimiter) const;
