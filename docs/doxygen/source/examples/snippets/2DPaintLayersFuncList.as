// добавить новый слой рисования
void AddPaintLayer( string );

// получить название текущего слоя для рисования
string GetCurrentPaintLayerName();

// переименовать текущий слой
void RenameCurrentPaintLayer( string );

// выбрать слой рисования по его названию
bool SelectPaintLayer( string )

// выбрать самый нижний слой рисования
void SelectFirstPaintLayer( bool )

// переместиться на след. слой рисования
bool SelectNextPaintLayer( bool )

// узнать, видим ли текущий слой рисования
bool GetPaintLayerVisibility()

// сделать слой рисования видимым (`true`) или скрыть его (`false`)
void SetPaintLayerVisibility( bool )

// добавить прозрачность (`0 .. 100`)
void SetPaintLayerOpacity( float )

// добавить глубину прозрачности (`0 .. 100`)
void SetPaintLayerDepthOpacity( float )
