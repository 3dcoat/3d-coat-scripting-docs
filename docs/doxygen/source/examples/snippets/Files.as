// доступ к отладке
Debug dbg;


void main() {

    // подготавливаем текстовый лог
    DebugLog log = dbg.log();
    log.clear();

    // предоставляет доступ к файлу "a.txt" через переменную `a`
    // обрати внимание: здесь файл создан не будет
    File  a( "a.txt" );
    if ( a.exists() ) {
        // удаляем существующий файл
        a.remove();
    }

    a += "Line for demo...\n"
         "and yet one, yes!";


    File  b( "b.data" );
    b.binary( true );
    string  ext = b.extension();
    log += "extension for `b` is " + ext;

    File  c( "my/folder/c.txt" );
    string fullPath = c.fullPathFile();
    log += "full path for `c` is " + fullPath;

    string path = c.path();
    log += "path for `c` is " + path;
    
    FormatBinary  bin = { 0x46, 0x4c, 0x49, 0x46 };
    b += bin;
    
    //b.remove();
}
