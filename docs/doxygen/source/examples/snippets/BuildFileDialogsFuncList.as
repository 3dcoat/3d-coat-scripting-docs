// Show open file dialog. List extensions like in example - *.tga;*.jpg;
// The resulting file name will be placed in result.
// Function returns true if file is successfully chosen.
DEPRECATED bool OpenDialog(string &in extensions,string &out result);

// Show save file dialog. List extensions like in example - *.tga;*.jpg;
// The resulting file name will be placed in result.
// Function returns true if file is successfully chosen.
DEPRECATED bool SaveDialog(string &in extensions,string &out result);

// Show selecting folder dialog.  The resulting file name
// will be placed in result.
// Function returns true if file is successfully chosen.
DEPRECATED bool FolderDialog(string &out result);

// Some functions in 3DCoat may engage file dialog that
// will wait user's input.
// You may skip that dialogs and provide file name
// automatically so that file dialog will return specified
// file name without waiting for user's input.
// Use empty name "" to stop automatic file dialogs skipping
// othervice all next file dialogs will be skipped and it may
// lead to loosing data.
DEPRECATED void SetFileForFileDialogs(string &in name);

// Returns true if Cancel pressed in the last file dialog.
DEPRECATED bool FileDialogCancelPressed();
