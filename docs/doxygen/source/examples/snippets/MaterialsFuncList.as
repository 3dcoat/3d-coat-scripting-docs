// добавить на сцену новый материал
DEPRECATED int AddMaterial( string );

// посчитать материалы на сцене
DEPRECATED int GetMaterialsCount()

// узнать название материала по его индексу
DEPRECATED string GetMaterialName( int )

// узнать индекс материала по его названию
DEPRECATED int GetMaterialIndex( string );

// переименовать материал
DEPRECATED void RenameMaterial( int index, string );

// удалить материал
DEPRECATED void DeleteMaterial( int index )

// заблокировать или разблокировать материал
DEPRECATED void LockMaterial( int index, bool )

// сделать материал видимым (`true`) или скрыть (`false`)
DEPRECATED void SetMaterialVisibility( int index, bool )


// посчитать объекты на сцене
DEPRECATED int GetObjectsCount()

// получить название объекта по его индексу
DEPRECATED string GetObjectName( int index )

// переименовать объект
DEPRECATED void RenameObject( int index, string );

// удалить объект
DEPRECATED void DeleteObject( int index )

// заблокировать или разблокировать объект
DEPRECATED void LockObject( int index, bool )

// сделать объект видимым (`true`) или скрыть (`false`)
DEPRECATED void SetObjectVisibility( int index, bool )

// посчитать количество граней на сцене
DEPRECATED int GetFacesCount();

// посчитать UV-развёртки на сцене
DEPRECATED int GetUVSetsCount();

// узнать название UV-развёртки по её индексу
DEPRECATED string GetUVSetName( int index );

// переименовать UV-развёртку
DEPRECATED void RenameUVSet( int index, string );

// выбрать все поверхности в текущей UV-развёртке
DEPRECATED void SelectAllFacesInCurrentUVSet();

// выбрать все поверхности в текущей UV-развёртке и текущей ретопо-группе
DEPRECATED void SelectAllFacesInCurrentUVSetAndGroup();

// выбрать все видимые поверхности в текущей UV-развёртке
DEPRECATED void SelectAllVisibleFaces();
