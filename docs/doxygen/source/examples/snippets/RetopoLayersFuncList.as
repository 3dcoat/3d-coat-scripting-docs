// добавить новый слой ретопологии (ретопо-слоя)
DEPRECATED void AddPaintLayer( string );

// получить название текущего ретопо-слоя
DEPRECATED string GetCurrentRetopoLayerName();

// переименовать слой
DEPRECATED void RenameCurrentRetopoLayer( string );

// выбрать ретопо-слой по его названию
DEPRECATED bool SelectRetopoLayer( string )

// выбрать самый нижний ретопо-слой
DEPRECATED void SelectFirstRetopoLayer( bool )

// переместиться на след. ретопо-слой
DEPRECATED bool SelectNextRetopoLayer( bool )

// узнать, видим ли текущий ретопо-слой
DEPRECATED bool GetRetopoLayerVisibility()

// сделать видимым (`true`) или скрыть (`false`) текущий ретопо-слой
DEPRECATED void SetRetopoLayerVisibility( bool )
