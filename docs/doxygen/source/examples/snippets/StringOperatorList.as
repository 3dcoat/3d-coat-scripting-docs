// The assignment operator copies the content of the right hand
// string into the left hand string.
// Assignment of primitive types is allowed, which will do a
// default transformation of the primitive to a string.
= assignment

// The concatenation operator appends the content of the right
// hand string to the end of the left hand string.
// Concatenation of primitives types is allowed, which will do
// a default transformation of the primitive to a string.
+, += concatenation

// Compares the content of the two strings.
==, != equality

// Compares the content of the two strings. The comparison is
// done on the byte values in the strings, which may not
// correspond to alphabetical comparisons for some languages.
<, >, <=, >= comparison

// The index operator gives access to a single byte in the string.
[] index operator
