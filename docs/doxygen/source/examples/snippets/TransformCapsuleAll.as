    // меняем позицию, поворачиваем и масштабируем капсулу
    mesh.tools().transform()
      .position( Vec3( 100, 50, 200 ) )
      .rotation( Angles( 30, 90, 200 ) )
      .scale( Vec3( 2, 1.5, 0.7 ) )
      .run();
