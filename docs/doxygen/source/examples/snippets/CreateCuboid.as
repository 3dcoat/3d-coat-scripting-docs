void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим кубоид
    Builder builder;
    Mesh mesh = builder.cuboid()
      .side( Vec3( 100, 80, 60 ) )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем кубоид на сцене
    room += mesh;
}
