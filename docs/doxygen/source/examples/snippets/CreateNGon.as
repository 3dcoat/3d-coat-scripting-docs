void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим энгон
    Builder builder;
    Mesh mesh = builder.ngon()
      .startPoint( Vec3( 0, 0, 0 ) )
      .endPoint( Vec3( 90, 90, 90 ) )
      .topRadius( 30 )
      .bottomRadius( 40 )
      .relativeHoleRadius( 0.3 )
      // количество сторон энгона
      .teeth( 3 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем энгон на сцене
    room += mesh;
}
