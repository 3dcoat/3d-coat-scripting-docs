// доступ к воксельному дереву
Vox v;


void main() {

    // строим дерево v-слоёв
    // см. `CreateVoxelTree.as`
    // ...

    // доступ по индексу для всех деток слоя "Head"
    v.to( "Body" ).call( "invert" );
    for ( int i = 0; i < v.count(); ++i ) {
        v.at( i ).call( "invert" ).parent();
        Wait( 100 );
    }
}




// функция для `forEach()`, см. выше
void invert() {
    v.isSurface() ? v.toVoxel() : v.toSurface();
}
