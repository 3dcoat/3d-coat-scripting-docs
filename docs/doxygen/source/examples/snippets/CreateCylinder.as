void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим цилиндр
    Builder builder;
    Mesh mesh = builder.cylinder()
      .positionTop( Vec3( 80, 0, 0 ) )
      .positionBottom( Vec3( 0, 0, 0 ) )
      .radiusTop( 40 )
      .radiusBottom( 50 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем цилиндр на сцене
    room += mesh;
}
