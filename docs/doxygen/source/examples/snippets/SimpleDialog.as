// простой счётчик времени
int t;


void main() {

    t = 0;

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // эта функция будет вызвана при появлении окна
    SetModalDialogCallback( "dialogCallback" );
    // отображаем окно с простым сообщением
    ModalDialog( "Some message", "Title" );
}




void dialogCallback() {

    // нажмём на кнопку в окне через несколько секунд после его открытия
    if (t++ > 400) {
      UI ui;
      ui( "$DialogButton#1" );
    }
}
