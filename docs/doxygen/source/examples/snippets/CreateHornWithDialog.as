void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // параметры по умолчанию
    float angle = 10.0;
    int numberOfChunks = 20;

    // окно с параметрами
    AddTranslation( "numberOfChunks", "Number of chunks in horn" );
    AddIntInput( "numberOfChunks", true );
    AddTranslation( "angle", "Angle" );
    AddFloatInput( "angle", true );
    bool ok = ModalDialogOkCancel( "Enter the horn parameters", "Create horn" );
    if ( !ok ) {
        // прекращаем выполнение скрипта
        return;
    }

    // строим фигуру с заданными выше параметрами
    ResetPrimTransform();
    PrimDensity( 0.3 );
    for ( int i = 0; i < numberOfChunks; i++ ) {
        capsule( -15, 0, 0, 15, 0, 0, 20, 20, 0 );
        PrimRotateY( 0, 0, 0, angle );
        PrimRotateX( 0, 0, 0, angle );
        PrimTranslate( 0, 15, 0 );
        PrimScaleAt( 10, 20, 30, 0.9, 0.9, 0.9 );
        ProgressBar( "Please wait", (i * 100) / numberOfChunks );
    }
}
