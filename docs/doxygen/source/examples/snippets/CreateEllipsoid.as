void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим эллипсоид
    Builder builder;
    Mesh mesh = builder.ellipsoid()
      .radius( Vec3( 80, 60, 40 ) )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем эллипсоид на сцене
    room += mesh;
}
