void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим шестерёнку
    Builder builder;
    Mesh mesh = builder.gear()
      .startPoint( Vec3( 0, 0, 0 ) )
      .endPoint( Vec3( 90, 90, 90 ) )
      .topRadius( 30 )
      .bottomRadius( 50 )
      .relativeHoleRadius( 0.3 )
      // насколько глубоко зубья приближаются к внутреннему радиусу
      .depth( 0.5 )
      // острота зубьев, 0 - самые колючие
      .sharpness( 0.2 )
      // количество зубьев
      .teeth( 3 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем шестерёнку на сцене
    room += mesh;
}
