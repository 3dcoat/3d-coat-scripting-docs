void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // строим капсулу
    Builder builder;
    Mesh mesh = builder.capsule()
      .startPosition( Vec3( 0 ) )
      .endPosition( Vec3( 40, 50, 60 ) )
      .startRadius( 30 )
      .endRadius( 50 )
      // детализация сетки, 0 < d <= 1.0
      .details( 0.1 )
      .build();

    // размещаем капсулу на сцене
    room += mesh;
}
