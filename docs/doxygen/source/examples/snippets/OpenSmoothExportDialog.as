// счётчик файлов
int n;
// список файлов
string files;

// доступ к интерфейсу пользователя
UI ui;


void main(){

    n = 0;
    files = "";

    // вызовем функцию `fileProcessing()` для каждого файла
    // по маске `mask` из выбранной папки
    // фильтр для выборки файлов
    string mask = "*.stl;*.obj;*.fbx";
    ForEachFileInFolder( "", mask, "fileProcessing" );
    // по завершению покажем что обработали
    ModalDialog( "Files processed:\n" + files, "" );
}




void fileProcessing( string &in fileName ) {

    ++n;

    // 
    SetModalDialogCallback( "callbackDontSave" );
    
    // создадим новую сцену
    ui( "$CLEARSCENE" );

    // сохраняем имя файла для след. диалогов
    SetFileForFileDialogs( fileName );

    // импортируем модель для вокселизации
    ui( "$ImportForVoxelizing" );

    // This is intended to press OK when user will be
    // asked if scale should be kept
    SetModalDialogCallback( "callbackOK" );

    // устанавлиаем, нужна ли вокселизация импортируемой модели
    ui( "$VoxelSculptTool::MergeWithoutVoxelizing", false )
      ( "*Apply" );

    // сглаживаем модель
    ui( "$[Page4]Smooth all" );
    
    // меняем расширение файла
    string fn = RemoveExtension( fileName );
    fn += "-smoothed.stl";
    
    // сохраняем имя файла для след. диалогов
    SetFileForFileDialogs( fn );
    
    // set output filename
    SetModalDialogCallback( "callbackDecimation" );

    // экспортируем сцену
    ui( "$ExportScene" );
}




void callbackDontSave() {
    // нажимаем `Don't save`
    ui( "$DialogButton#2" );
}


void callbackOK() {
    // нажимаем `OK`
    ui( "$DialogButton#1" );
}


void callbackDecimation() {
    // устанавливаем уровень уменьшения и нажимаем `OK`
    SetSliderValue( "$DecimationParams::ReductionPercent", 80 );
    ui( "$DialogButton#1" );
}
