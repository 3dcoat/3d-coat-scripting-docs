// Concatenates the strings in the array into a large string,
// separated by the delimiter.
string join(const array<string> &in arr, const string &in delimiter)

// Parses the string for an integer value. The base can be 10 or 16
// to support decimal numbers or hexadecimal numbers. If byteCount
// is provided it will be set to the number of bytes that were
// considered as part of the integer value.
int parseInt(const string &in str, uint base = 10, uint &out byteCount = 0)
uint parseUInt(const string &in str, uint base = 10, uint &out byteCount = 0)

// Parses the string for a floating point value. If byteCount is
// provided it will be set to the number of bytes that were considered
// as part of the value.
double parseFloat(const string &in, uint &out byteCount = 0)

// The format functions takes a string that defines how the number should
// be formatted. The string is a combination of the following characters:
//     l = left justify
//     0 = pad with zeroes
//     + = always include the sign, even if positive
//     space = add a space in case of positive number
//     h = hexadecimal integer small letters (not valid for formatFloat)
//     H = hexadecimal integer capital letters (not valid for formatFloat)
//     e = exponent character with small e (only valid for formatFloat)
//     E = exponent character with capital E (only valid for formatFloat)
string formatInt(int val, const string &in options = '', uint width = 0)
string formatUInt(uint val, const string &in options = '', uint width = 0)
string formatFloat(double val, const string &in options = '', uint width = 0, uint precision = 0)
