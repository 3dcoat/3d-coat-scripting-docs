﻿void main() {

    // подготавливаем сцену
    SculptRoom room;
    room.clear().toSurface();

    // создаём две сферы разного размера рядом так,
    // чтобы они перекрывали друг друга
    Builder builder;
    Mesh a = builder.sphere()
      .radius( 70 )
      .details( 0.1 )
      .build();
    Mesh b = builder.sphere()
      .radius( 40 )
      .position( Vec3( 30, 40, 50 ) )
      .details( 0.5 )
      .build();


    // выполняем булевы операции над мешами
    // и помещаем результат на сцену
    const Vec3 floorShift( 0, 200, 0 );
    
    Mesh add = a | b;
    Vec3 floor = Vec3( 0 );
    add.tools().transform().position( floor ).run();
    room += add;

    Mesh subtract = a - b;
    floor += floorShift;
    subtract.tools().transform().position( floor ).run();
    room += subtract;

    Mesh intersect = a & b;
    floor += floorShift;
    intersect.tools().transform().position( floor ).run();
    room += intersect;
}
