﻿Documentation for the [3DCoat's](http://3dcoat.com/) scripting. Load and run!

Since version [4.5.32](http://3dcoat.com/forum/index.php?showtopic=17076).

For update docs on Wndows just run `build Doxy.cmd` from folder `doxygen/build`.

Edit the file `Doxy` in any text editor for
[configure docs](http://www.doxygen.nl/manual/faq.html).
